//
//  Date+Conversion.swift
//  CHISI
//
//  Created by Dima Panychyk on 10/23/16.
//  Copyright © 2016 mt53bureau. All rights reserved.
//

import SwiftDate

extension Date {

    /// Converts Date to DateInRegion applying current device's timezone
    func toCurrentDateInRegion() -> DateInRegion {
        let region = Region(tz: TimeZoneName.current, cal: CalendarName.gregorian, loc: LocaleName.current)
        return DateInRegion(absoluteDate: self, in: region)
    }
}
