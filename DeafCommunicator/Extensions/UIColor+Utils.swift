//
// Created by Mykolaj on 16.11.16.
//

import Foundation

extension UIColor {
    /// This function can be called like this UIColor.colorWithRGBHex(0xFF6622)
    /// which allows to pass a color mocking HTML notation
    static func colorWithRGBHex (_ hex: Int) -> UIColor {
        let r: Int = (hex >> 16) & 0xFF
        let g: Int = (hex >> 8) & 0xFF
        let b: Int = (hex) & 0xFF

        return UIColor(colorLiteralRed: Float(r) / Float(255),
                green: Float(g) / Float(255),
                blue: Float(b) / Float(255),
                alpha: 1.0)
    }
}