//
// Created by A. Mykolaj on 11.10.16.
//

import Foundation

enum FontSize: Int {
    case k_8 = 8
    case k_9 = 9
    case k_10 = 10
    case k_11 = 11
    case k_12 = 12
    case k_14 = 14
    case k_16 = 16
    case k_18 = 18
    case k_20 = 20
    case k_22 = 22
    case k_24 = 24
    case k_26 = 26
    case k_28 = 28
    case k_36 = 36
    case k_48 = 48
    case k_72 = 72

    func increase() -> FontSize {
        switch self {
        case .k_8 :
            return .k_9
        case .k_9 :
            return .k_10
        case .k_10 :
            return .k_11
        case .k_11 :
            return .k_12
        case .k_12 :
            return .k_14
        case .k_14 :
            return .k_16
        case .k_16 :
            return .k_18
        case .k_18 :
            return .k_20
        case .k_20 :
            return .k_22
        case .k_22 :
            return .k_24
        case .k_24 :
            return .k_26
        case .k_26 :
            return .k_28
        case .k_28 :
            return .k_36
        case .k_36 :
            return .k_48
        case .k_48 :
            return .k_72
        case .k_72 :
            return self
        default:
            return .k_18
        }
    }

    func decrease() -> FontSize {
        switch self {
        case .k_8 :
            return self

        case .k_9 :
            return .k_8

        case .k_10 :
            return .k_9

        case .k_11 :
            return .k_10

        case .k_12 :
            return .k_11

        case .k_14 :
            return .k_12

        case .k_16 :
            return .k_14

        case .k_18 :
            return .k_16

        case .k_20 :
            return .k_18

        case .k_22 :
            return .k_20

        case .k_24 :
            return .k_22

        case .k_26 :
            return .k_24

        case .k_28 :
            return .k_26

        case .k_36 :
            return .k_28

        case .k_48 :
            return .k_36

        case .k_72 :
            return .k_48

        default:
            return .k_18

        }
    }

    var pixelSize: Int {
        return self.rawValue
    }

    static func fromPixelSize(size: Int) -> FontSize {
        switch size {
        case k_8.rawValue :
            return k_8

        case k_9.rawValue :
            return .k_9

        case k_10.rawValue :
            return .k_10

        case k_11.rawValue :
            return .k_11

        case k_12.rawValue :
            return .k_12

        case k_14.rawValue :
            return .k_14

        case k_16.rawValue :
            return .k_16

        case k_18.rawValue :
            return .k_18

        case k_20.rawValue :
            return .k_20

        case k_22.rawValue :
            return .k_22

        case k_24.rawValue :
            return .k_24

        case k_26.rawValue :
            return .k_26

        case k_28.rawValue :
            return .k_28

        case k_36.rawValue :
            return .k_36

        case k_48.rawValue :
            return .k_48

        case k_72.rawValue :
            return .k_72

        default:
            return .k_18
        }
    }
}
