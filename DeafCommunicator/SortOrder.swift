//
// Created by Mykolaj on 12.12.16.
//

import Foundation

enum SortOrder {
    case asc
    case desc
}
