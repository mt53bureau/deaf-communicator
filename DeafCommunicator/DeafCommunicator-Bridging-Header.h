//
//  DeafCommunicator-Bridging-Header.h
//  CHISI
//
//  Created on 30.09.16.
//

#ifndef DeafCommunicator_Bridging_Header_h
#define DeafCommunicator_Bridging_Header_h

#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEAcousticModel.h>
#import <OpenEars/OEPocketsphinxController.h>
#import <OpenEars/OEEventsObserver.h>
#import <OpenEars/OELogging.h>
#import <OpenEars/OEFliteController.h>
#import <Slt/Slt.h>
#import <SharkORM/SharkORM.h>
#import <HockeySDK/HockeySDK.h>
#import "Widgets/DrawingTool/LinearInterpView.h"
#import <BEMCheckBox/BEMCheckBox.h>

#endif /* DeafCommunicator_Bridging_Header_h */
