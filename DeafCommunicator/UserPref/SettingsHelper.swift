//
// Created by Antony Mykolaj on 18.10.16.
// Copyright (c) 2016 mt53bureau. All rights reserved.
//

import Foundation

final class SettingsHelper {

    private static let prefKeySpeechTextSize = "speechTextSize"
    private static let prefKeySpeechEngineType = "speechEngineType"

    private init() {
    }

    static func saveSpeechTextSize(size: FontSize!) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.set(Int(size.pixelSize), forKey: SettingsHelper.prefKeySpeechTextSize)
        defaults.synchronize()
    }

    static func getSpeechTextSize() -> FontSize {
        let defaults: UserDefaults = UserDefaults.standard
        let pt = defaults.integer(forKey: SettingsHelper.prefKeySpeechTextSize)
        return FontSize.fromPixelSize(size: pt)
    }

    static func saveSpeechRecognizerType(_ recognizer: SpeechRecognizerType) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.set(recognizer.rawValue, forKey: SettingsHelper.prefKeySpeechEngineType)
        defaults.synchronize()
    }

    static func getSpeechRecognizerType() -> SpeechRecognizerType {
        let defaults: UserDefaults = UserDefaults.standard
        let integer = defaults.integer(forKey: SettingsHelper.prefKeySpeechEngineType)

        if integer == nil {
            return SpeechRecognizerType.openEars
        }

        switch integer {
        case SpeechRecognizerType.openEarsRecognizerCode:
            return SpeechRecognizerType.openEars
        case SpeechRecognizerType.appleIosRecognizerCode:
            return SpeechRecognizerType.appleIos10
        default:
            return SpeechRecognizerType.openEars
        }
    }
}
