//
// Created by A Mykolaj on 15.10.16.
//

import Foundation
import SwiftDate

final class CsvHelper {

    private init() {
    }

    static func putRecord(_ record: Phrase, conversation: Conversation) {
//        let region = Region(tz: TimeZoneName.current, cal: CalendarName.gregorian, loc: LocaleName.current)
        let gmtRegion = Region(tz: TimeZoneName.gmt, cal: CalendarName.gregorian, loc: LocaleName.englishUnitedStates)
        let date = Date(timeIntervalSince1970: Double(record.timestamp))  // UTC time
        let csvDateStr = DateInRegion(absoluteDate: date, in: gmtRegion).string(format: .custom("yyyy-MM-dd'T'HH:mm:ss.SSSZ"))
        let patientId = record.patientId as String
        let conversationId = Int(conversation.id as NSNumber)

        // A line to wright into a CSV file
        let csvRecord = "\(patientId);\(csvDateStr);\(record.text as String)\n"

        let fnDate = Date(timeIntervalSince1970: Double(record.timestamp))  // UTC time
        let fnDateStr = fnDate.string(format: .custom("yyyyMMdd-HHmmss"), in: gmtRegion)
        let fileName = "\(fnDateStr)_\(patientId)_\(conversationId).csv"

        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let filePath = dir.appendingPathComponent(fileName)

            // Check if file exists
            if let fileHandle = FileHandle(forWritingAtPath: filePath.absoluteString) {
                // Append record to existing file
                fileHandle.seekToEndOfFile()
                fileHandle.write(csvRecord.data(using: String.Encoding.utf8)!)
                fileHandle.closeFile()
            } else {
                // Create new file
                do {
                    try csvRecord.write(to: filePath, atomically: false, encoding: String.Encoding.utf8)
                    print("\(fileName) -> : \(csvRecord)")
                } catch {
                    print("CSV write error: \(error)")
                }
            }
        }
    }

}
