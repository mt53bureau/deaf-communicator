//
// Created by tony on 06.10.16.
// Copyright (c) 2016 mt53bureau. All rights reserved.
//

import Foundation

final class OsVersion {

    private init() {
    }

    /// Checks if current iOS version is at least iOS 10 or later
    static func isEqualOrLaterIos10() -> Bool {
        if #available(iOS 10.0, *) {
            return true
        } else {
            return false
        }
    }

    static func isIos8AtLeast() -> Bool {
        if #available(iOS 8.0, *) {
            return true
        } else {
            return false
        }
    }
}
