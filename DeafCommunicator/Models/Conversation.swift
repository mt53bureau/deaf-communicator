//
// Created by tony on 04.10.16.
// Copyright (c) 2016 mt53bureau. All rights reserved.
//

import Foundation
import SharkORM

class Conversation: SRKObject {
    dynamic var patientId : String!
    dynamic var startTime : NSNumber!
    dynamic var endTime : NSNumber?

    /// As one of requirements we have one that states thad after deleting a record from a database we have to update
    /// conversation id's to always start from 1. This field serves for that purpose.
    dynamic var displayId : NSNumber!
}

