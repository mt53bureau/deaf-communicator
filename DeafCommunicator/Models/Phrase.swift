//
// Created by tony on 06.10.16.
// Copyright (c) 2016 mt53bureau. All rights reserved.
//

import Foundation
import SharkORM

class Phrase: SRKObject {
    dynamic var text : String!
    dynamic var timestamp : NSNumber!
    dynamic var conversationId: NSNumber!
    dynamic var patientId : String!
}
