//
// Created by Antony Mykolaj on 27.10.16.
// Copyright (c) 2016 mt53bureau. All rights reserved.
//

import Foundation
import SwiftDate

class ConversationWrapper {

    private(set) var conversation: Conversation
    private(set) var startTime: DateInRegion
    var conversationId: Int {
        get {
            return Int(conversation.id)
        }
    }
    var patientId: String {
        get {
            return conversation.patientId
        }
    }
    var displayId: Int {
        get {
            return Int(conversation.displayId)
        }
    }
    private(set) var phrases: [Phrase] = [Phrase]()
    var isChecked: Bool = false

    init (_ conversation: Conversation) {
        self.conversation = conversation
        self.startTime = Date(timeIntervalSince1970: Double(conversation.startTime)).toCurrentDateInRegion()
    }

    /// Returns all phrases of this conversation as a single string
    var conversationContent: String {
        var content: String = ""
        for (i, phrase) in phrases.enumerated() {
            content += "\(phrase.text as String)."
            if i < phrases.count - 1 {
                content += " "
            }
        }
        return content
    }

    final func addPhrase(_ phrase: Phrase) {
        phrases.append(phrase)
    }
}
