//
//  AboutViewController.swift
//  Chisi
//
//  Created by Mykolaj on 05.12.16.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var btnDismiss: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var dialogContentFrame: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Make background to occupy whole screen
        shadowView.frame = UIScreen.main.bounds
        // Bind buttons to actions
        btnDismiss.addTarget(self, action: #selector(onDismissButtonClick(_:)), for: .touchDown)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        dialogContentFrame.layer.cornerRadius = 8
        dialogContentFrame.layer.masksToBounds = true
    }


    public func onDismissButtonClick(_ sender: UIButton) {
        dismiss(animated: false)
    }

}
