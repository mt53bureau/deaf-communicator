//
//  DateViewController.swift
//  CHISI
//
//  Created by Dima Panychyk on 10/22/16.
//

import UIKit
import SwiftDate

protocol DateViewControllerDelegate {
    func didSetDate(year: Int, month: Int, day: Int)
}

class DateViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPopoverControllerDelegate {

    public var beginTime: Date!
    public var preSelectedDate: Date?

    private let hardcodedHeight: Int = 256
    private let hardcodedWidth: Int = 320

    var delegate: DateViewControllerDelegate?

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Check if everything is properly set up
        precondition(beginTime != nil, "You've forget to pass all needed parameters to this ViewController")

        // Sed size of this pop-up
        preferredContentSize = preferredSize
        setDateRange(beginTime)
    }

    // MARK: - Actions

    @IBAction func searchAction(_ sender: UIButton) {
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day], from: datePicker.date)

        guard let year = dateComponents.year, let month = dateComponents.month, let day = dateComponents.day else {
            fatalError("Something's wrong with your date")
        }

        delegate?.didSetDate(year: year, month: month, day: day)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func exitAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    //MARK: - Private

    private func setDateRange(_ beginDate: Date) {
        datePicker.minimumDate = beginDate

        if let selectedDate = preSelectedDate {
            // In case there is any date already selected we set that date as an active one in a DatePicker
            datePicker.date = selectedDate
        } else {
            // In other case we highlight a today's date in a DatePicker
            datePicker.date = Date()
            preSelectedDate = datePicker.date
        }

        // Do not allow a user to select a date greater than today's date
        datePicker.maximumDate = Date()
    }

    private var preferredSize: CGSize {
        return CGSize(width: CGFloat(preferredMenuWidth), height: CGFloat(preferredMenuHeight))
    }

    private var preferredMenuWidth: Int {
        // TODO In case you find this size inappropriate you may feel free and calculate it based on a current screen width. I don't mind.
        return hardcodedWidth
    }

    private var preferredMenuHeight: Int {
        // TODO In case you find this size inappropriate you may feel free and calculate it based on a current screen height. I don't mind.
        return hardcodedHeight
    }

}
