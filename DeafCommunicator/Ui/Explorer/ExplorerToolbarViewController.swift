//
//  ExplorerToolbarViewController.swift
//  CHISI
//
//  Created by Mykolaj on 10.12.16.
//

import UIKit

class ExplorerToolbarViewController: UIViewController {

    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnSelectAll: UIButton!

    var delegate: ExplorerToolbarDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        btnDelete.addTarget(self, action: #selector(onDeleteButtonClick(_:)), for: .touchDown)
        btnSelectAll.addTarget(self, action: #selector(onSelectAllButtonClick(_:)), for: .touchDown)
    }

    func onDeleteButtonClick(_ button: UIButton) {
        delegate?.onDeleteButtonClicked(self)
    }

    func onSelectAllButtonClick(_ button: UIButton) {
        delegate?.onSelectAllButtonClicked(self)
    }
}

protocol ExplorerToolbarDelegate {
    func onDeleteButtonClicked(_ viewController: ExplorerToolbarViewController)
    func onSelectAllButtonClicked(_ viewController: ExplorerToolbarViewController)
}
