//
//  ExplorerViewController.swift
//  CHISI
//
//  Created by A. Mykolaj on 06.10.16.
//

import UIKit
import SwiftDate
import SharkORM
import SnapKit

class ExplorerViewController: UIViewController {

    fileprivate static let SHOW_DATE_SEGUE_ID = "showDateSegueId"
    fileprivate static let PHRASE_CELL_ID = "explorerPhraseCell"
    fileprivate static let EMBED_TOOLBAR_SEGUE_ID = "explorerToolbarEmbedSegueId"
    fileprivate static let CELL_EXTRA_WIDTH_MULTIPLIER = CGFloat(1.1)

    /// Tracks a currently selected date
    fileprivate var selectedDate: Date?

    /// Tracks if a user has selected any specific date to show conversations for
    fileprivate var currentDisplayMode: DisplayMode = .displayModeDateNotSpecified

    /// A worker to do a work in the background
    fileprivate let backgroundQueue = DispatchQueue(label: "\(type(of: self)).dbIoQueue", qos: .background, target: nil)

    /// Array of conversations
    fileprivate private(set) var conversations = [ConversationWrapper]()

    /// Timestamp of oldest conversation available
    fileprivate var oldestConversationDate: Date?

    /// Maximum existing ID for a conversation (used for a table column width calculation)
    fileprivate var maxConversationId: Int? {
        willSet {
            // Recalculate how much space is needed to draw a new value on screen
            var strSize: CGSize
            if let id = newValue {
                strSize = calculateDrawingBoundsForText(String(id), font: conversationIdTableCellFont)
            } else {
                strSize = calculateDrawingBoundsForText("0", font: conversationIdTableCellFont)
            }
            conversationIdTableCellSize = strSize
        }
    }

    /// Longest user ID (used for a table column width calculation)
    fileprivate var longestPatientId: String? {
        willSet {
            // Recalculate how much space is needed to draw a new value on screen
            var strSize: CGSize
            if let idStr = newValue {
                strSize = calculateDrawingBoundsForText(idStr, font: defaultTableCellFont)
            } else {
                strSize = calculateDrawingBoundsForText("0", font: defaultTableCellFont)
            }
            patientIdTableCellSize = strSize
        }
    }
    fileprivate var sortingCriteriaToOrderingMap: [SortingCriteria: SortOrder] = [
            .displayId: .asc,
            .patientId: .asc,
            .date: .asc
    ]

    fileprivate var conversationIdTableCellSize: CGSize = CGSize(width: 0, height: 0)
    fileprivate var patientIdTableCellSize: CGSize = CGSize(width: 0, height: 0)
    fileprivate var timestampTableCellSize: CGSize = CGSize(width: 0, height: 0)
    fileprivate let conversationIdTableCellFont = UIFont.systemFont(ofSize: 17.0)
    fileprivate let defaultTableCellFont = UIFont.systemFont(ofSize: 17.0)
    fileprivate let defaultColumnHeaderFont = UIFont.systemFont(ofSize: 13.0)
    fileprivate var minPatientIdCellSize: CGSize = CGSize(width: 0, height: 0)
    fileprivate var minConversationIdCellSize: CGSize = CGSize(width: 0, height: 0)

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSelectDate: UIButton!
    @IBOutlet weak var tableInfoBar: UIView!
    @IBOutlet weak var bottomPanelView: UIView!
    @IBOutlet weak var columnHeaderView: UIView!
    @IBOutlet weak var btnConversationIdColHeader: UIButton!
    @IBOutlet weak var btnPatientIdColHeader: UIButton!
    @IBOutlet weak var btnDateCellHeader: UIButton!
    @IBOutlet weak var conversationTextCellHeader: UILabel!
    fileprivate var btnExport: UIBarButtonItem!

    @IBAction func onSelectDateButtonClicked(_ sender: AnyObject) {
        performSegue(withIdentifier: ExplorerViewController.SHOW_DATE_SEGUE_ID, sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        timestampTableCellSize = calculateDrawingBoundsForText("9999/99/99 99:99:99 AM", font: defaultTableCellFont)
        minConversationIdCellSize = calculateDrawingBoundsForText("Serial No", font: defaultColumnHeaderFont)
        minPatientIdCellSize = calculateDrawingBoundsForText("Patient Id", font: defaultColumnHeaderFont)

        // Add Export button to a navigation bar
        btnExport = UIBarButtonItem(title: "Export", style: .plain, target: self, action: #selector(onExportButtonClick(_:)))
        navigationItem.rightBarButtonItem = btnExport

        // Bottom toolbar should be hidden from the start
        bottomPanelView.isHidden = true

        // Configure a table
        tableView.tableHeaderView = columnHeaderView
        tableView.rowHeight = 80
        if let header = tableView.tableHeaderView {
            var f: CGRect = header.frame
            f.size.height = 50
            header.frame = f
        }
        // Table headers
        btnConversationIdColHeader.setContentHuggingPriority(1000, for: .horizontal)
        btnConversationIdColHeader.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().priority(1000)
            make.centerY.equalToSuperview()
            make.width.greaterThanOrEqualTo(66)
        }
        btnPatientIdColHeader.setContentHuggingPriority(1000, for: .horizontal)
        btnPatientIdColHeader.snp.makeConstraints { (make) in
            make.leading.equalTo(btnConversationIdColHeader.snp.trailing).offset(4).priority(1000)
            make.centerY.equalToSuperview()
            make.width.greaterThanOrEqualTo(66)
        }
        btnDateCellHeader.setContentHuggingPriority(1000, for: .horizontal)
        btnDateCellHeader.snp.makeConstraints { (make) in
            make.leading.equalTo(btnPatientIdColHeader.snp.trailing).offset(4).priority(1000)
            make.centerY.equalToSuperview()
            make.width.greaterThanOrEqualTo(66)
        }
        conversationTextCellHeader.setContentHuggingPriority(50, for: .horizontal)
        conversationTextCellHeader.snp.makeConstraints { (make) in
            make.leading.equalTo(btnDateCellHeader.snp.trailing).offset(4)
            make.trailing.equalToSuperview().priority(250)
            make.centerY.equalToSuperview()
        }

        // Click handlers for column headers
        btnConversationIdColHeader.addTarget(self, action: #selector(onHeaderConversationIdClick(_:)), for: .touchDown)
        btnPatientIdColHeader.addTarget(self, action: #selector(onHeaderPatientIdClick(_:)), for: .touchDown)
        btnDateCellHeader.addTarget(self, action: #selector(onHeaderDateClick(_:)), for: .touchDown)
        
        let borderColor = UIColor.colorWithRGBHex(0xbcbcbc).cgColor
        columnHeaderView.layer.borderColor = borderColor
        columnHeaderView.layer.borderWidth = 1.0
        
        reloadDisplayedData()
    }

    func onExportButtonClick(_ button: UIBarButtonItem) {
        button.isEnabled = false
        backgroundQueue.async {
            let fileUrl = self.exportConversationsToCsv()
            DispatchQueue.main.async {
                button.isEnabled = true
                if let url = fileUrl {
                    let documentController = UIDocumentInteractionController(url: url)
                    documentController.presentOptionsMenu(from: self.btnSelectDate.frame, in: self.view, animated: true)
                }
            }
        }
    }

    fileprivate func reloadDisplayedData() {
        // Perform database I/O in the background thread // TODO Show a progress-bar while a query is in progress
        backgroundQueue.async {
            var result: [ConversationWrapper]
            if self.currentDisplayMode == .displayModeDateNotSpecified {
                result = ConversationDbMethods.getConversations()
            } else {
                precondition(self.selectedDate != nil, "Selected date must not be null here")
                result = ConversationDbMethods.getConversations(self.selectedDate)
            }

            // Also get the oldest conversation date for later use
            self.oldestConversationDate = ConversationDbMethods.getOldestConversationDate()
            self.maxConversationId = ConversationDbMethods.getMaxConversationId()
            self.longestPatientId = ConversationDbMethods.getLongestPatientId()

            DispatchQueue.main.async {
                debugPrint("Max conversation ID '\(self.maxConversationId)'")
                debugPrint("Longest patient ID '\(self.longestPatientId != nil ? self.longestPatientId : "")'")

                self.conversations.removeAll()
                for c in result {
                    self.conversations.append(c)
                }
                self.syncBtnSelectDateLabel()
                self.sortTableRows(.displayId, ordering: .desc)
                self.tableView.reloadData()
            }
        }
    }

    private func syncBtnSelectDateLabel() {
        let region = Region(tz: TimeZoneName.current, cal: CalendarName.gregorian, loc: LocaleName.current)
        // Change a label for a SelectDate button
        if let date = self.selectedDate {
            // Pizza time. I haven't found a way to add space between icon and label other than to use "space" char
            let str = date.string(format: .custom(" " + "EEEE, MMM. dd yyyy"), in: region)
            self.btnSelectDate.setTitle(str, for: [])
        } else {
            // Pizza time 2
            self.btnSelectDate.setTitle(" " + "Select Date", for: [])
        }
        self.btnSelectDate.sizeToFit()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ExplorerViewController.SHOW_DATE_SEGUE_ID {
            let vc = (segue.destination as! DateViewController)
            vc.delegate = self
            vc.preSelectedDate = selectedDate
            vc.beginTime = oldestConversationDate != nil ? oldestConversationDate : Date()

            // This pop-up often gets positioned in a wrong position.
            // Here i set it to be shown right where it should be.
            let r: CGRect = btnSelectDate.frame
            vc.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: r.width, height: r.height)
        } else if segue.identifier == ExplorerViewController.EMBED_TOOLBAR_SEGUE_ID {
            let vc = (segue.destination as! ExplorerToolbarViewController)
            vc.delegate = self

        }
    }

    fileprivate func log10(_ value: Double) -> Double {
        return log(value) / log(10.0)
    }

    /// Calculates a size of rectangle to display a given text on a screen
    fileprivate func calculateDrawingBoundsForText(_ text: String, font: UIFont) -> CGSize {
        let textAttr = [NSFontAttributeName: font]
        let size = text.size(attributes: textAttr)
        return size
    }

    fileprivate func exportConversationsToCsv() -> URL? {
        let result: [ConversationWrapper] = ConversationDbMethods.getConversations()
        let sortedResult = result.sorted(by: {
            $0.startTime.isBefore(date: $1.startTime, granularity: Calendar.Component.second)
        })

        // Create file
        let gmtRegion = Region(tz: TimeZoneName.gmt, cal: CalendarName.gregorian, loc: LocaleName.englishUnitedStates)
        let fnDate = Date()
        let fnDateStr = fnDate.string(format: .custom("yyyyMMdd-HHmmss"), in: gmtRegion)
        let fileName = "\(fnDateStr).csv"

        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let filePath = dir.appendingPathComponent(fileName)

            // Check if file exists
            if let fileHandle = FileHandle(forWritingAtPath: filePath.absoluteString) {
                print("File \(filePath) already exists. Truncating...")
                do {
                    try fileHandle.truncateFile(atOffset: 0)
                    try fileHandle.closeFile()
                    print("OK")
                } catch {
                    print("Error: \(error)")
                    return nil
                }
            }

            let header = "Conversation ID;Patient ID;Date;Text"
            do {
                try header.write(to: filePath, atomically: false, encoding: String.Encoding.utf8)
            } catch {
                print("CSV write error: \(error)")
                return nil
            }

            for conversation in sortedResult {
                let date = conversation.startTime.absoluteDate // UTC time
                let csvDateStr = DateInRegion(absoluteDate: date, in: gmtRegion).string(format: .custom("yyyy-MM-dd HH:mm:ss.SSSZ"))
                let patientId = conversation.patientId as String
                let conversationId = conversation.displayId

                var text = ""
                for phrase in conversation.phrases {
                    text = text + phrase.text + ". "
                }

                // A line to wright into a CSV file
                let csvRecord = "`\(conversationId)`;`\(patientId)`;`\(csvDateStr)`;`\(text)`\n"
                do {
                    try csvRecord.write(to: filePath, atomically: false, encoding: String.Encoding.utf8)
                    print("\(fileName) -> : \(csvRecord)")
                } catch {
                    print("CSV write error: \(error)")
                    return nil
                }
            }
            return filePath
        }
        return nil
    }

    fileprivate func removeConversations(_ list: [ConversationWrapper]) {
        if list.isEmpty {
            return
        }

        // After removing a conversation we need to update displayId of other conversations
        let sortedList = list.sorted(by: { c0, c1 in
            c0.displayId < c1.displayId
        })
        guard let cw = list.first else {
            return
        }
        // Remember a displayId of the first record to be deleted so we know where to start from when updating records
        let firstDisplayId = cw.displayId

        // Remove all selected conversations
        for c in sortedList {
            // At first remove all phrases in a current conversation from a database
            for p in c.phrases {
                let _ = ConversationDbMethods.removePhrase(p.id as Int)
            }
            // Now remove a conversation itself
            let _ = ConversationDbMethods.removeConversation(c.conversationId)
        }

        // Select all records
        let queryResult = ConversationDbMethods.getConversations()

        var nextDisplayId: Int = 1
        for record in queryResult {
            let cId = (record as! ConversationWrapper).conversationId
            ConversationDbMethods.updateConversationDisplayId(cId, newDisplayId: nextDisplayId)
            nextDisplayId = nextDisplayId + 1
        }
    }

//    fileprivate func setButtonStateForSortOrder() {
//        btnConversationIdColHeader.setImage(UIImage(named: "ic_order_ascending_32pt.png"), for:[])
//        btnConversationIdColHeader.titleEdgeInsets = UIEdgeInsetsMake(0.0, btnConversationIdColHeader.center.x/2 , 0.0, 0.0);
//        btnConversationIdColHeader.contentHorizontalAlignment =  .left
//    }

    fileprivate func sortTableRows(_ criteria: SortingCriteria, ordering: SortOrder) {
        switch criteria {
        case .displayId:
            switch ordering {
            case .desc:
                conversations.sort(by: { c0, c1 in c0.displayId > c1.displayId })
                sortingCriteriaToOrderingMap[.displayId] = .desc
            case .asc:
                conversations.sort(by: { c0, c1 in c0.displayId < c1.displayId })
                sortingCriteriaToOrderingMap[.displayId] = .asc
            }

        case .patientId:
            switch ordering {
            case .desc:
                conversations.sort(by: { c0, c1 in
                    if c0.patientId == c1.patientId {
                        return c0.conversationId > c1.conversationId
                    }
                    return c0.patientId.localizedStandardCompare(c1.patientId) == .orderedDescending
                })
                sortingCriteriaToOrderingMap[.patientId] = .desc

            case .asc:
                conversations.sort(by: { c0, c1 in
                    if c0.patientId == c1.patientId {
                        return c0.conversationId > c1.conversationId
                    }
                    return c0.patientId.localizedStandardCompare(c1.patientId) == .orderedAscending
                })
                sortingCriteriaToOrderingMap[.patientId] = .asc
            }

        case .date:
            switch ordering {
            case .desc:
                conversations.sort(by: { c0, c1 in
                    Int(c0.conversation.startTime) > Int(c1.conversation.startTime)
                })
                sortingCriteriaToOrderingMap[.date] = .desc
            case .asc:
                conversations.sort(by: { c0, c1 in
                    Int(c0.conversation.startTime) < Int(c1.conversation.startTime)
                })
                sortingCriteriaToOrderingMap[.date] = .asc
            }

        default:
            fatalError("Sorting criteria not handled")
            break

        }
        tableView.reloadData()
    }

    func onHeaderConversationIdClick(_ b: AnyObject) {
        let order = sortingCriteriaToOrderingMap[.displayId]
        if order == .asc {
            sortTableRows(.displayId, ordering: .desc)
        } else {
            sortTableRows(.displayId, ordering: .asc)
        }
    }

    func onHeaderPatientIdClick(_ b: AnyObject) {
        let order = sortingCriteriaToOrderingMap[.patientId]
        if order == .asc {
            sortTableRows(.patientId, ordering: .desc)
        } else {
            sortTableRows(.patientId, ordering: .asc)
        }
    }

    func onHeaderDateClick(_ b: AnyObject) {
        let order = sortingCriteriaToOrderingMap[.date]
        if order == .asc {
            sortTableRows(.date, ordering: .desc)
        } else {
            sortTableRows(.date, ordering: .asc)
        }
    }
}

// MARK: - DateViewControllerDelegate

extension ExplorerViewController: DateViewControllerDelegate {

    func didSetDate(year: Int, month: Int, day: Int) {
        currentDisplayMode = DisplayMode.displayModeDateSelected
        var cmp = DateComponents()
        cmp.timeZone = TimeZone.current
        cmp.calendar = CalendarName.gregorian.calendar
        cmp.calendar?.locale = Calendar.current.locale
        cmp.year = year
        cmp.month = month
        cmp.day = day
        cmp.hour = 0
        cmp.minute = 0
        selectedDate = try! DateInRegion(components: cmp).absoluteDate
        reloadDisplayedData()
    }
}

extension ExplorerViewController: UITableViewDelegate {

//    //
//    // The huinya через ці методи ширина строчок в табличці різна при кожному оновленні даних.
//    // Це заважає вичислити висоту/ширину ячейки і вручну виставити розміри колонок
//    //
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as! PhraseCell
//        if cell.labelPhraseContent.isTruncated() {
//            cell.labelPhraseContent.numberOfLines = 0
//            tableView.beginUpdates()
//            tableView.endUpdates()
//        }
//    }
////
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as! PhraseCell
//
//        if cell.labelPhraseContent.numberOfLines == 0 {
//            cell.labelPhraseContent.numberOfLines = 2
//            tableView.beginUpdates()
//            tableView.endUpdates()
//        }
//    }
////
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
//
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }

}

extension ExplorerViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.conversations.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item: ConversationWrapper = self.conversations[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: ExplorerViewController.PHRASE_CELL_ID,
                for: indexPath) as! ExplorerTableViewCell

        guard let labelDate = cell.labelDate else {
            fatalError("Check a cell view")
        }
        guard let labelPatientId = cell.labelPatientId else {
            fatalError("Check a cell view")
        }
        guard let labelPhraseContent = cell.labelPhraseContent else {
            fatalError("Check a cell view")
        }
        guard let labelConversationId = cell.labelConversationId else {
            fatalError("Check a cell view")
        }

        labelDate.text = item.startTime.string(format: .custom("yyyy/MM/dd h:mm:ss a"))
        labelPhraseContent.text = item.conversationContent
        labelConversationId.text = String(item.displayId)
        labelPatientId.text = item.patientId
        cell.checkBox.on = item.isChecked

        // Conversation ID cell
        labelConversationId.snp.makeConstraints { make in
            if minConversationIdCellSize.width <= conversationIdTableCellSize.width {
                make.width.equalTo(conversationIdTableCellSize.width * ExplorerViewController.CELL_EXTRA_WIDTH_MULTIPLIER).priority(1000)
            } else {
                make.width.equalTo(minConversationIdCellSize.width * ExplorerViewController.CELL_EXTRA_WIDTH_MULTIPLIER).priority(1000)
            }
        }

        // Patient ID cell
        labelPatientId.snp.makeConstraints { make in
            if minPatientIdCellSize.width <= patientIdTableCellSize.width {
                make.width.equalTo(patientIdTableCellSize.width * ExplorerViewController.CELL_EXTRA_WIDTH_MULTIPLIER).priority(1000)
            } else {
                make.width.equalTo(minPatientIdCellSize.width * ExplorerViewController.CELL_EXTRA_WIDTH_MULTIPLIER).priority(1000)
            }
        }

        // Timestamp cell
        labelDate.snp.makeConstraints { make in
            make.width.equalTo(timestampTableCellSize.width * ExplorerViewController.CELL_EXTRA_WIDTH_MULTIPLIER).priority(1000)
        }

        cell.onCheckBoxChecked = { index in
            debugPrint("Record with id \(item.conversationId) is checked")
            item.isChecked = true

            // In case we have at least one record checked a bottom toolbar should became visible
            if self.bottomPanelView.isHidden {
                self.bottomPanelView.isHidden = false
            }
        }
        cell.onCheckBoxUnchecked = { index in
            debugPrint("Record with id \(item.conversationId) is unchecked")
            item.isChecked = false

            // Hide a bottom toolbar in case there aren't any records selected anymore
            let selectedConversations = self.conversations.filter() {
                return ($0 as ConversationWrapper).isChecked
            }
            if selectedConversations.isEmpty {
                self.bottomPanelView.isHidden = true
            }
        }

        return cell
    }
}

extension ExplorerViewController: ExplorerToolbarDelegate {

    func onDeleteButtonClicked(_ viewController: ExplorerToolbarViewController) {
        // Find selected conversations
        let selectedConversations = conversations.filter() {
            return ($0 as ConversationWrapper).isChecked
        }

        // This should not happen because a bottom panel should not be shown if there isn't at least one conversation
        // selected. But in case it's still happens we just hide the bottom panel.
        if selectedConversations.isEmpty {
            bottomPanelView.isHidden = false
            return
        }

        let recordWord = selectedConversations.count > 1 ? "records" : "record"
        let message = "Do you want to remove \(selectedConversations.count) selected records \(recordWord) from a database?" +
                "\nThis action can not be undone."

        let alert = UIAlertController(title: "Remove Conversations", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default,
                handler: { action in

                }))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default,
                handler: { action in
                    self.backgroundQueue.async {
                        self.removeConversations(selectedConversations)

                        DispatchQueue.main.async {
                            self.reloadDisplayedData()
                        }
                    }
                }))
        present(alert, animated: true, completion: nil)
    }

    func onSelectAllButtonClicked(_ viewController: ExplorerToolbarViewController) {
        // Find out which action do we need to perform: "Check all" or "Uncheck all"
        var hasAtLeastOneUnchecked = false
        for c in conversations {
            if !c.isChecked {
                hasAtLeastOneUnchecked = true
                break
            }
        }

        if hasAtLeastOneUnchecked {
            // Check all
            for c in conversations {
                c.isChecked = true
            }
        } else {
            // Uncheck all
            for c in conversations {
                c.isChecked = false
            }
        }
        tableView.reloadData()
    }
}

fileprivate enum DisplayMode: Int {
    case displayModeDateNotSpecified = 1
    case displayModeDateSelected = 2
}

fileprivate enum SortingCriteria {
    case displayId
    case patientId
    case date
}
