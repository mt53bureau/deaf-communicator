//
// Created by A. Mykolaj on 10.10.16.
//

import Foundation
import SnapKit
import BEMCheckBox

class ExplorerTableViewCell: UITableViewCell {

    @IBOutlet weak var labelPhraseContent: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelPatientId: UILabel!
    @IBOutlet weak var labelConversationId: UILabel!
    @IBOutlet weak var checkBox: BEMCheckBox!
    @IBOutlet weak var separator1: UIView!
    @IBOutlet weak var separator2: UIView!
    @IBOutlet weak var separator3: UIView!
    @IBOutlet weak var separator4: UIView!

    fileprivate let hSpacing = 8
    fileprivate let padding = 4

    var onCheckBoxChecked: (() -> Void)? = nil
    var onCheckBoxUnchecked: (() -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        labelPhraseContent.numberOfLines = 0
        checkBox.delegate = self
        labelConversationId.snp.makeConstraints { (make) -> Void in
            make.leading.equalTo(contentView).offset(padding).priority(1000)
            make.centerY.equalTo(contentView)
        }
        separator1.snp.makeConstraints({(make) -> Void in
            make.height.equalTo(contentView).priority(1000)
            make.leading.equalTo(labelConversationId.snp.trailing).offset(hSpacing / 2).priority(1000)
            make.width.equalTo(1).priority(1000)
        })
        labelPatientId.snp.makeConstraints { (make) -> Void in
            make.leading.equalTo(labelConversationId.snp.trailing).offset(hSpacing).priority(1000)
            make.centerY.equalTo(contentView)
        }
        separator2.snp.makeConstraints({(make) -> Void in
            make.height.equalTo(contentView).priority(1000)
            make.leading.equalTo(labelPatientId.snp.trailing).offset(hSpacing / 2).priority(1000)
            make.width.equalTo(1).priority(1000)
        })
        labelDate.snp.makeConstraints { (make) -> Void in
            make.leading.equalTo(labelPatientId.snp.trailing).offset(hSpacing).priority(1000)
            make.centerY.equalTo(contentView)
        }
        separator3.snp.makeConstraints({(make) -> Void in
            make.height.equalTo(contentView).priority(1000)
            make.leading.equalTo(labelDate.snp.trailing).offset(hSpacing / 2).priority(1000)
            make.width.equalTo(1).priority(1000)
        })
        labelPhraseContent.snp.makeConstraints { (make) -> Void in
            make.leading.equalTo(labelDate.snp.trailing).offset(hSpacing).priority(1000)
            make.centerY.equalTo(contentView)
            make.top.equalToSuperview().offset(padding)
            make.bottom.equalToSuperview().offset(padding * -1)
        }
        separator4.snp.makeConstraints({(make) -> Void in
            make.height.equalTo(contentView).priority(1000)
            make.leading.equalTo(labelPhraseContent.snp.trailing).offset(hSpacing / 2).priority(1000)
            make.width.equalTo(1).priority(1000)
        })
        checkBox.snp.makeConstraints { (make) -> Void in
            make.leading.equalTo(labelPhraseContent.snp.trailing).offset(hSpacing).priority(1000)
            make.trailing.equalTo(contentView).offset(padding * -1).priority(1000)
            make.width.equalTo(26).priority(1000)
            make.height.equalTo(26).priority(1000)
            make.centerY.equalTo(contentView)
        }
        let separatorColor = UIColor.colorWithRGBHex(0xbcbcbc)
        separator1.backgroundColor = separatorColor
        separator2.backgroundColor = separatorColor
        separator3.backgroundColor = separatorColor
        separator4.backgroundColor = separatorColor
    }

}

extension ExplorerTableViewCell: BEMCheckBoxDelegate {
    func didTap(_ checkBox: BEMCheckBox) {
        if checkBox.on {
            onCheckBoxChecked?()
        } else {
            onCheckBoxUnchecked?()
        }
    }

}
