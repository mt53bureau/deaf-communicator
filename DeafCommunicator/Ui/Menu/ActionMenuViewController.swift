//
//  ActionMenuViewController.swift
//  CHISI
//
//  Created by A. Mykolaj on 14.10.16.
//

import UIKit

protocol ActionMenuViewControllerDelegate {
    func onSettingsMenuSelected(_ viewController: ActionMenuViewController)

    func onExplorerMenuSelected(_ viewController: ActionMenuViewController)

    func onAboutMenuSelected(_ viewController: ActionMenuViewController)
}

class ActionMenuViewController: UIViewController {

    var delegate: ActionMenuViewControllerDelegate?

    private let menuItemHeight: Int = 48
    private let itemVerticalSpacing: Int = 8
    private let menuItemCount: Int = 3
    private let hardcodedWidth: Int = 256

    @IBOutlet weak var explorerMenuItem: UIButton!
    @IBOutlet weak var settingsMenuItem: UIButton!
    @IBOutlet weak var aboutMenuItem: UIButton!
    @IBOutlet weak var stackContainingMenuItems: UIStackView!

    override func viewDidLoad() {
        super.viewDidLoad()
        preferredContentSize = preferredSize

        // Bind buttons to actions
        explorerMenuItem.addTarget(self, action: #selector(onExplorerMenuClick(_:)), for: .touchDown)
        settingsMenuItem.addTarget(self, action: #selector(onSettingsMenuClick(_:)), for: .touchDown)
        aboutMenuItem.addTarget(self, action: #selector(onAboutMenuClick(_:)), for: .touchDown)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        configureViewDimensions()
    }

    private func configureViewDimensions() {
        explorerMenuItem.frame.size.height = CGFloat(menuItemHeight)
        settingsMenuItem.frame.size.height = CGFloat(menuItemHeight)
        aboutMenuItem.frame.size.height = CGFloat(menuItemHeight)
        stackContainingMenuItems.spacing = CGFloat(itemVerticalSpacing)
    }

    public func onSettingsMenuClick(_ sender: UIButton) {
        delegate?.onSettingsMenuSelected(self)
    }

    public func onExplorerMenuClick(_ sender: UIButton) {
        delegate?.onExplorerMenuSelected(self)
    }

    public func onAboutMenuClick(_ sender: UIButton) {
        delegate?.onAboutMenuSelected(self)
    }

    private var preferredSize: CGSize {
        let height = menuItemCount * menuItemHeight + (itemVerticalSpacing * (menuItemCount - 1))
        return CGSize(width: CGFloat(preferredMenuWidth), height: CGFloat(height))
    }

    private var preferredMenuWidth: Int {
        return hardcodedWidth
    }
}
