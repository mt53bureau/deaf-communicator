//
//  SessionDialogViewController.swift
//  CHISI
//
//  Created by Mykolaj on 03.10.16.
//

import UIKit

class SessionDialogViewController: UIViewController {

    @IBOutlet weak var patientIdInput: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var darkenView: UIView!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak private var constraintAlignHolderViewY: NSLayoutConstraint!

    var host: PatientIdDialogHost?

    override func viewDidLoad() {
        super.viewDidLoad()
        patientIdInput.delegate = self
        darkenView.frame = UIScreen.main.bounds
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerForNotifications(true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        patientIdInput.becomeFirstResponder()
    }


    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        registerForNotifications(false)
    }

    private func registerForNotifications(_ register: Bool) {
        let center = NotificationCenter.default
        if register {
            center.addObserver(self,
                    selector: #selector(keyboardWillShow(_:)),
                    name: .UIKeyboardWillShow,
                    object: nil)

            center.addObserver(self,
                    selector: #selector(keyboardWillHide(_:)),
                    name: .UIKeyboardWillHide,
                    object: nil)
        } else {
            center.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
            center.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
        }
    }

    private func checkTextInput() -> Bool {
        guard let text = patientIdInput.text else {
            return false
        }
        if text.isEmpty {
            return false
        }
        return true
    }

    // MARK: - Actions:

    @IBAction func onCancelButtonClick(_ sender: AnyObject) {
        host?.onPatientIdDialogCanceled(dialog: self)
    }

    @IBAction func onConfirmButtonClick(_ sender: AnyObject) {
        if !checkTextInput() {
            btnConfirm.isEnabled = false
            return
        }
        if let id = patientIdInput.text {
            host?.onPatientIdProvided(patientId: id, dialog: self)
        }
    }

    // MARK: - NotificationCenter

    func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            pullUpAlertOnKbShown(kbSize: keyboardSize.size)
        }
    }

    func keyboardWillHide(_ notification: Notification) {
        pullDownAlertOnKbHide()
    }

    private func pullUpAlertOnKbShown(kbSize size: CGSize) {
        let margin = CGFloat(10.0)
        let bottomMargin = ((view.bounds.size.height / 2) - (holderView.bounds.height / 2))
        let kbHeight = size.height
        if (kbHeight + margin) > bottomMargin { // дужки фіксять ібучий варнінг про асоціативність (він мене замахав)
            constraintAlignHolderViewY.constant = (-1) * ((kbHeight - bottomMargin) + margin)
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.view.layoutIfNeeded()
            })
        }
    }

    private func pullDownAlertOnKbHide() {
        self.constraintAlignHolderViewY.constant = 0
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
}

// MARK: - UITextFieldDelegate:

extension SessionDialogViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let patientId: String = textField.text!
        
        // About to insert the very first character
        if patientId.characters.count == 0 && !string.isEmpty {
            btnConfirm.isEnabled = true
        }
        // About to remove the last character
        else if patientId.characters.count == 1 && string.isEmpty {
            btnConfirm.isEnabled = false
        }
        // No input
        else if patientId.isEmpty {
            btnConfirm.isEnabled = false
        }
        // Nothing of the above
        else {
            btnConfirm.isEnabled = true
        }

        if string == "\n" && patientId.isEmpty {
            btnConfirm.isEnabled = false
        }

        if string == "\n" && !patientId.isEmpty {
            host?.onPatientIdProvided(patientId: patientId, dialog: self)
            textField.resignFirstResponder()
            btnConfirm.isEnabled = true
        }
        return true
    }
}
