//
//  QuestionAnswerViewController.swift
//  CHISI
//
//  Created by Mykolaj on 16.11.16.
//

import UIKit

class QuestionAnswerViewController: UIViewController {

    @IBOutlet weak var btnPositiveAnswer: UIButton!
    @IBOutlet weak var btnNegativeAnswer: UIButton!
    var delegate: QuestionAnswerViewControllerDelegate?

    
    @IBAction func onPositiveButtonClick(_ sender: AnyObject) {
        delegate?.onPositiveAnswer(self)
    }

    @IBAction func onNegativeButtonClick(_ sender: AnyObject) {
        delegate?.onNegativeAnswer(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        preferredContentSize = preferredSize
        popoverPresentationController?.delegate = self
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5.0
        view.layer.shadowColor = UIColor.black.cgColor;
        view.layer.shadowOffset = CGSize(width: 0, height: -1)
        view.layer.shadowOpacity = 0.6
        let shadowPath: UIBezierPath  = UIBezierPath(roundedRect: view.layer.bounds, cornerRadius: CGFloat(8.0))
        view.layer.shadowPath = shadowPath.cgPath
        btnPositiveAnswer.layer.cornerRadius = 8.0
        btnNegativeAnswer.layer.cornerRadius = 8.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    var preferredSize: CGSize {
        let screenRect: CGRect = UIScreen.main.bounds
        let screenWidth: CGFloat = screenRect.size.width
        let screenHeight: CGFloat = screenRect.size.height

        let w = screenWidth - (screenWidth / 5)
        let h = CGFloat(88.0)
        return CGSize(width: w, height: h)
    }
}

extension QuestionAnswerViewController: UIPopoverPresentationControllerDelegate {
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
}

protocol QuestionAnswerViewControllerDelegate {

    /// Indicates that a user has answered "Yes"
    func onPositiveAnswer(_ viewController: QuestionAnswerViewController)

    /// Indicates that a user has answered "No"
    func onNegativeAnswer(_ viewController: QuestionAnswerViewController)
}
