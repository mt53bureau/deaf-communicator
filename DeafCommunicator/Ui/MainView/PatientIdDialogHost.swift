//
//  PatientIdDialogHost.swift
//  CHISI
//
//  Created by Mykolaj on 03.10.16.
//

import Foundation

protocol PatientIdDialogHost {
    func onPatientIdDialogCanceled(dialog: SessionDialogViewController)
    func onPatientIdProvided(patientId: String, dialog: SessionDialogViewController)
}
