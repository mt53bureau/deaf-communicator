//
//  RecognitionViewController.swift
//  CHISI
//
//  Created by Mykolaj on 25.09.16.
//

import UIKit
import AVFoundation
import SwiftDate
import Darwin

class RecognitionViewController: UIViewController {

    fileprivate static let LOG_TAG = "RecognitionViewController"
    fileprivate static let showPatientIdDialogSegueId = "showPatientIdDialogSegue"
    fileprivate static let showExplorerSegueId = "showExplorerSegue"
    fileprivate static let showActionBarMenuSegueId = "showActionBarMenuSegue"
    fileprivate static let showSettingSegueId = "showSettingSegueId"
    fileprivate static let showQuestionAnswerSegueId = "showQuestionAnswerSegueId"
    fileprivate static let showGoodnessLevelSegueId = "showGoodnessLevelSegueId"
    fileprivate static let questionAnswerViewControllerId = "QuestionAnswerViewController"
    fileprivate static let painLevelViewControllerId = "PainLevelViewController"
    fileprivate static let showAboutWindowSegueId = "showAboutWindowSegue"

    fileprivate static let STATUS_TEXT_VIEW_TAG: Int = 1
    fileprivate static let RECOGNIZED_TEXT_VIEW_TAG: Int = 2
    fileprivate static let BUTTON_EDIT_RECOGNIZED_TEXT_TAG: Int = 3

    fileprivate var isConversationInProgress: Bool = false
    fileprivate var isRecordingInProgress: Bool = false
    fileprivate var currentConversation: Conversation!
    fileprivate let bgndQueue = DispatchQueue(label: "\(type(of: self)).dbIoQueue", qos: .background, target: nil)
    fileprivate var currentFontSize: FontSize! = .k_18
    fileprivate var speechProcessor: SpeechProcessorImpl!
    fileprivate var currentPhrase: Phrase?

    @IBOutlet weak var statusTextView: UILabel!
    @IBOutlet weak var btnStartConversation: UIButton!
    @IBOutlet weak var recognizedSpeechTextView: UITextView!
    @IBOutlet weak var btnDecreaseTextSize: UIButton!
    @IBOutlet weak var btnIncreaseTextView: UIButton!
    @IBOutlet weak var sideToolBar: UIView!
    @IBOutlet weak var btnShowPaintTool: UIButton!
    @IBOutlet weak var btnShowGoodnessTool: UIButton!
    @IBOutlet weak var btnShowAnswerTool: UIButton!
    @IBOutlet weak var goodnessToolView: UIView!
    @IBOutlet weak var answerToolView: UIView!
    @IBOutlet weak var drawingToolView: UIView!
    @IBOutlet weak var btnEditRecognizedText: UIButton!
    @IBOutlet weak var btnStartStopRecord: UIButton!

    @IBAction func onDecreaseTextSizeButtonClick(_ sender: AnyObject) {
        setSpeechTextSize(newSize: currentFontSize.decrease())
    }

    @IBAction func onIncreaseTextSizeButtonClick(_ sender: AnyObject) {
        setSpeechTextSize(newSize: currentFontSize.increase())
    }

    func onStartConversationButtonClick(_ sender: AnyObject) {
        if !isConversationInProgress {
            if !speechProcessor.checkMicrophonePermissions() {
                speechProcessor.requestAuthorization(self)
                return
            }
            // Show a dialog where a user can enter a patient ID
            performSegue(withIdentifier: RecognitionViewController.showPatientIdDialogSegueId, sender: self)
        } else {
            toggleConversation(false)
        }
    }

    @IBAction func onShowAnswerToolButtonClick(_ sender: AnyObject) {
        if answerToolView.isHidden {
            answerToolView.isHidden = false
            if let image = UIImage(named: "ic_question_tool_checked_48pt.png") {
                btnShowAnswerTool.setImage(image, for: [])
            }
        } else {
            answerToolView.isHidden = true
            if let image = UIImage(named: "ic_question_tool_48pt.png") {
                btnShowAnswerTool.setImage(image, for: [])
            }
        }
    }

    @IBAction func onShowGoodnessToolButtonClick(_ sender: AnyObject) {
        if goodnessToolView.isHidden {
            goodnessToolView.isHidden = false
            if let image = UIImage(named: "ic_goodness_tool_checked_48pt.png") {
                btnShowGoodnessTool.setImage(image, for: [])
            }
        } else {
            goodnessToolView.isHidden = true
            if let image = UIImage(named: "ic_goodness_tool_48pt.png") {
                btnShowGoodnessTool.setImage(image, for: [])
            }
        }
    }

    @IBAction func onShowDrawingToolButtonClick(_ sender: AnyObject) {
        if drawingToolView.isHidden {
            drawingToolView.isHidden = false
            if let image = UIImage(named: "ic_paint_tool_checked_48pt.png") {
                btnShowPaintTool.setImage(image, for: [])
            }
        } else {
            drawingToolView.isHidden = true
            if let image = UIImage(named: "ic_paint_tool_48pt.png") {
                btnShowPaintTool.setImage(image, for: [])
            }
        }
    }

    @IBAction func onEditRecognizedTextButtonClick(_ sender: AnyObject) {
        if !recognizedSpeechTextView.isEditable {
            recognizedSpeechTextView.isEditable = true
            recognizedSpeechTextView.becomeFirstResponder()
        } else {
            recognizedSpeechTextView.isEditable = false
            recognizedSpeechTextView.resignFirstResponder()

            // Update a text in a Phrase object after the text is edited
            guard let phrase = currentPhrase else {
                preconditionFailure("Current phrase can not be null at this point. You've forget to set the variable" +
                        " when a user clicked \"Edit\" button")
            }
            phrase.text = recognizedSpeechTextView.text
            bgndQueue.async {
                phrase.commit()
            }
        }
    }

    func onStartStopRecordButtonClick(_ sender: AnyObject) {
        toggleRecording(!isRecordingInProgress)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        statusTextView.tag = RecognitionViewController.STATUS_TEXT_VIEW_TAG
        btnEditRecognizedText.tag = RecognitionViewController.BUTTON_EDIT_RECOGNIZED_TEXT_TAG
        recognizedSpeechTextView.tag = RecognitionViewController.RECOGNIZED_TEXT_VIEW_TAG

        setSpeechTextSize(newSize: currentFontSize)

        let recognizerType = SettingsHelper.getSpeechRecognizerType()
        switch recognizerType {
        case SpeechRecognizerType.openEars:
            speechProcessor = OpenEarsSpeechProcessor()
            speechProcessor.delegate = self

        case SpeechRecognizerType.appleIos10:
            speechProcessor = AppleNativeSpeechProcessor()
            speechProcessor.delegate = self

        default:
            fatalError("Wrong speech processor (Why?)")
        }

        // These pop-up views should be invisible on start
        goodnessToolView.isHidden = true
        answerToolView.isHidden = true
        drawingToolView.isHidden = true

        // "Edit" button should be hidden too
        btnEditRecognizedText.isHidden = true

        // "Start/Stop" record button should be hidden to
        btnStartStopRecord.isHidden = true

        // Bind buttons to actions
        btnStartStopRecord.addTarget(self, action: #selector(onStartStopRecordButtonClick(_:)), for: .touchDown)
        btnStartConversation.addTarget(self, action: #selector(onStartConversationButtonClick(_:)), for: .touchDown)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Maybe a user has changed a speech recognizer in settings
        if !isConversationInProgress {
            let selectedType = SettingsHelper.getSpeechRecognizerType()
            if speechProcessor.speechRecognizerType != selectedType {
                switch selectedType {
                case SpeechRecognizerType.openEars:
                    speechProcessor = OpenEarsSpeechProcessor()
                    speechProcessor.delegate = self

                case SpeechRecognizerType.appleIos10:
                    speechProcessor = AppleNativeSpeechProcessor()
                    speechProcessor.delegate = self

                default:
                    fatalError("No speech processor selected (Why?)")
                }
            }
        }

        registerForKeyboardNotifications(true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        registerForKeyboardNotifications(false)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // Stop listening when the app goes into a background or gets hidden
        if isConversationInProgress {
            toggleConversation(false)
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        sideToolBar.roundCorners(corners: [.bottomLeft, .topLeft], radius: 4.0)
    }

    private func registerForKeyboardNotifications(_ register: Bool) {
        let center = NotificationCenter.default
        if register {
            center.addObserver(self,
                    selector: #selector(keyboardWillShow(_:)),
                    name: .UIKeyboardWillShow,
                    object: nil)

            center.addObserver(self,
                    selector: #selector(keyboardWillHide(_:)),
                    name: .UIKeyboardWillHide,
                    object: nil)
        } else {
            center.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
            center.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
        }
    }

    func keyboardWillShow(_ notification: Notification) {

        // Here we set content inset for a text field so a keyboard to not overlap a text
        guard let info = notification.userInfo else {
            return
        }
        guard let v = info[UIKeyboardFrameBeginUserInfoKey] else {
            return
        }
        let kbSize: CGSize = (v as! NSValue).cgRectValue.size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
        recognizedSpeechTextView.contentInset = contentInsets
    }

    func keyboardWillHide(_ notification: Notification) {
        // Reset text inset after a keyboard is hidden
        recognizedSpeechTextView.contentInset = UIEdgeInsets.zero
    }

    fileprivate func toggleConversation(_ enable: Bool) {
        if enable && isConversationInProgress {
            print("A conversation is already in progress.")
            return
        }

        if enable {
            precondition(currentConversation != nil)
            precondition(speechProcessor.checkMicrophonePermissions())

            // Reflect in UI that a conversation is now started
            isConversationInProgress = true
            btnStartConversation.setImage(UIImage(named: "ic_stop_48pt.png"), for: [])
            btnStartStopRecord.isHidden = false
            btnStartStopRecord.setImage(UIImage(named: "ic_microphone_idle_56pt.png"), for: [])
            recognizedSpeechTextView.text = ""
        } else {
            currentConversation?.endTime = DateInRegion().absoluteDate.timeIntervalSince1970 as NSNumber
            bgndQueue.async {
                self.currentConversation?.commit()

                DispatchQueue.main.async {
                    debugPrint("Conversation '\(self.currentConversation.id as Int)' is now stopped")
                    self.speechProcessor.stopListening()
                    self.btnStartConversation.setImage(UIImage(named: "ic_play_48pt.png"), for: [])
                    self.btnStartStopRecord.isHidden = true
                    self.btnStartStopRecord.setImage(UIImage(named: "ic_microphone_idle_56pt.png"), for: [])
                    self.btnEditRecognizedText.isHidden = true
                    self.isConversationInProgress = false
                    self.isRecordingInProgress = false
                }
            }
        }
    }

    fileprivate func toggleRecording(_ enable: Bool) {
        if enable && isRecordingInProgress {
            debugPrint("Speech recognition is already in progress.")
            return
        }

        if enable {
            precondition(currentConversation != nil)
            precondition(speechProcessor.checkMicrophonePermissions())
            btnEditRecognizedText.isHidden = true

            if speechProcessor.startListening(currentConversation) {
                btnStartStopRecord.setImage(UIImage(named: "ic_microphone_enabled_56pt.png"), for: [])
                isRecordingInProgress = true
            }
        } else {
            speechProcessor.stopListening()
            btnStartStopRecord.setImage(UIImage(named: "ic_microphone_idle_56pt.png"), for: [])
            isRecordingInProgress = false
        }
    }

    private func setSpeechTextSize(newSize: FontSize) {
        currentFontSize = newSize
        let currentFont: UIFont! = recognizedSpeechTextView.font

        // 'Editable' flag is needed for new font size to be set
        recognizedSpeechTextView.isEditable = true

        recognizedSpeechTextView.font = UIFont(name: currentFont.fontName, size: CGFloat(currentFontSize.pixelSize))
        recognizedSpeechTextView.setNeedsLayout()
        recognizedSpeechTextView.layoutIfNeeded()

        // Disable editing after the font size is changed because we do not need this field to be editable
        recognizedSpeechTextView.isEditable = false
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == RecognitionViewController.showPatientIdDialogSegueId {
            let patientIdDialog = (segue.destination as! SessionDialogViewController)
            patientIdDialog.host = self
        } else if segue.identifier == RecognitionViewController.showActionBarMenuSegueId {
            let vc = (segue.destination as! ActionMenuViewController)
            vc.delegate = self
        }
    }

}

// MARK: - Patient ID input dialog

extension RecognitionViewController: PatientIdDialogHost {

    func onPatientIdDialogCanceled(dialog: SessionDialogViewController) {
        dialog.dismiss(animated: false)
        isConversationInProgress = false
    }

    func onPatientIdProvided(patientId: String, dialog: SessionDialogViewController) {
        currentConversation = Conversation()
        currentConversation.patientId = patientId
        currentConversation.startTime = DateInRegion().absoluteDate.timeIntervalSince1970 as NSNumber

        bgndQueue.async {
            // Find out which display ID to assign to this new conversation
            if let maxId = ConversationDbMethods.getConversationMaxDisplayId() {
                self.currentConversation.displayId = (maxId + 1) as NSNumber
            } else {
                self.currentConversation.displayId = 1 as NSNumber
            }

            self.currentConversation.commit()
            DispatchQueue.main.async {
                self.toggleConversation(true)
                debugPrint("Conversation '\(self.currentConversation.id as Int)' is started")
            }
        }
        dialog.dismiss(animated: false)
    }
}

// MARK: - ActionMenuViewControllerDelegate:

extension RecognitionViewController: ActionMenuViewControllerDelegate {
    func onSettingsMenuSelected(_ viewController: ActionMenuViewController) {
        viewController.dismiss(animated: false)
        if isConversationInProgress {
            let alert = UIAlertController(title: "Stop Conversation", message: "Before you can go to Settings you need to stop a current session.\n\nDo you want to stop current session and proceed to Settings?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default,
                    handler: { action in
                        viewController.dismiss(animated: false)
                    }))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default,
                    handler: { action in
                        self.toggleRecording(false)
                        viewController.dismiss(animated: false)
                        self.performSegue(withIdentifier: RecognitionViewController.showSettingSegueId, sender: self)
                    }))
            present(alert, animated: true, completion: nil)
        } else {
            performSegue(withIdentifier: RecognitionViewController.showSettingSegueId, sender: self)
        }
    }

    func onExplorerMenuSelected(_ viewController: ActionMenuViewController) {
        viewController.dismiss(animated: false)
        if isConversationInProgress {
            let alert = UIAlertController(title: "Stop Conversation", message: "Before you can go to Explorer you need to stop a current session.\n\nDo you want to stop current session and proceed to Explorer?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default,
                    handler: { action in
                        viewController.dismiss(animated: false)
                    }))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default,
                    handler: { action in
                        self.toggleRecording(false)
                        viewController.dismiss(animated: false)
                        self.performSegue(withIdentifier: RecognitionViewController.showExplorerSegueId, sender: self)
                    }))
            present(alert, animated: true, completion: nil)
        } else {
            performSegue(withIdentifier: RecognitionViewController.showExplorerSegueId, sender: self)
        }
    }

    func onAboutMenuSelected(_ viewController: ActionMenuViewController) {
        viewController.dismiss(animated: false)
        performSegue(withIdentifier: RecognitionViewController.showAboutWindowSegueId, sender: self)
    }
}

extension RecognitionViewController: QuestionAnswerViewControllerDelegate {
    func onPositiveAnswer(_ viewController: QuestionAnswerViewController) {
    }

    func onNegativeAnswer(_ viewController: QuestionAnswerViewController) {
    }
}

extension RecognitionViewController: SpeechProcessorDelegate {

    func onSuggestionUpdate(text: String) {
        precondition(currentConversation != nil, "Conversation must not be null at this point")

        bgndQueue.async {
            var conversationText = ""
            let phrases = ConversationDbMethods.getTalk(self.currentConversation.id as Int)
            for (i, phrase) in phrases.enumerated() {
                conversationText += phrase.text
                conversationText += ".\n\n"
            }

            DispatchQueue.main.async {
                conversationText += text
                self.recognizedSpeechTextView.text = conversationText + "\n\n"
                // Scroll recognized text to bottom
                if !conversationText.isEmpty {
                    let range: NSRange = NSMakeRange(conversationText.characters.count - 1, 1)
                    self.recognizedSpeechTextView.scrollRangeToVisible(range)
                }
            }
        }
    }

    func onFinalSuggestion(text: String) {
        precondition(currentConversation != nil, "Conversation must not be null at this point")

        if text.isEmpty {
            return
        }

        // Save conversation to a database
        let phrase = Phrase()
        phrase.conversationId = self.currentConversation.id
        phrase.patientId = self.currentConversation.patientId
        phrase.timestamp = Date().timeIntervalSince1970 as NSNumber
        phrase.text = text

        // Remember this frase in case user wants to edit it
        currentPhrase = phrase

        // Show Edit button so user can edit a recognized text in case he's not sattisfied with the result
        btnEditRecognizedText.isHidden = false

        self.bgndQueue.async {
            phrase.commit()

            var conversationText = ""
            let phrases = ConversationDbMethods.getTalk(self.currentConversation.id as Int)
            for (i, phrase) in phrases.enumerated() {
                conversationText += phrase.text
                if i == phrases.count - 1 {
                    conversationText += "."
                } else {
                    conversationText += ".\n\n"
                }
            }
            DispatchQueue.main.async {
                self.recognizedSpeechTextView.text = conversationText

                // Scroll recognized text to bottom
                if !conversationText.isEmpty {
                    let range: NSRange = NSMakeRange(conversationText.characters.count - 1, 1)
                    self.recognizedSpeechTextView.scrollRangeToVisible(range)
                }
            }
        }
    }

    func onStatusUpdate(text: String) {
        statusTextView.text = text
    }

}
