//
//  GoodnessLevelViewController.swift
//  DeafCommunicator
//
//  Created by Mykolaj on 16.11.16.
//

import UIKit

class GoodnessLevelViewController: UIViewController {

    @IBOutlet var levelIndicator: GoodnessView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preferredContentSize = preferredSize
        popoverPresentationController?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupShadow()
    }

    private func setupShadow() {
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5.0
        view.layer.shadowColor = UIColor.black.cgColor;
        view.layer.shadowOffset = CGSize(width: 0, height: -1)
        view.layer.shadowOpacity = 0.6
        let shadowPath: UIBezierPath  = UIBezierPath(roundedRect: view.layer.bounds, cornerRadius: CGFloat(8.0))
        view.layer.shadowPath = shadowPath.cgPath
    }

    private func setupLevelIndicatorView() {
        let startingColorOfGradient = UIColor.colorWithRGBHex(0x548B54).cgColor
        let endingColorOFGradient = UIColor.colorWithRGBHex(0xCD0000).cgColor
        let gradient: CAGradientLayer = CAGradientLayer()

        let origin = levelIndicator.frame.origin
        let size = levelIndicator.frame.size
        gradient.frame = levelIndicator.frame
        gradient.startPoint = CGPoint(x: origin.x, y: origin.y)
        gradient.endPoint = CGPoint(x: origin.x + size.width, y: origin.y + size.height)
        gradient.colors = [startingColorOfGradient , endingColorOFGradient]
        levelIndicator.layer.insertSublayer(gradient, at: 0)
    }


    var preferredSize: CGSize {
        let screenRect: CGRect = UIScreen.main.bounds
        let screenWidth: CGFloat = screenRect.size.width
        let screenHeight: CGFloat = screenRect.size.height

        let w = screenWidth - (screenWidth / 5)
        let h = CGFloat(120.0)
        return CGSize(width: w, height: h)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GoodnessLevelViewController: UIPopoverPresentationControllerDelegate {
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
}
