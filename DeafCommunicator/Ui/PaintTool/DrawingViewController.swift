//
//  DrawingViewController.swift
//  CHISI
//
//  Created by Mykolaj on 18.11.16.
//

import UIKit
import TGPControls

class DrawingViewController: UIViewController {

    fileprivate let RED_BUTTON_TAG = 11
    fileprivate let ORANGE_BUTTON_TAG = 12
    fileprivate let YELLOW_BUTTON_TAG = 13
    fileprivate let GREEN_BUTTON_TAG = 14
    fileprivate let BLUE_BUTTON_TAG = 15
    fileprivate let INDIGO_BUTTON_TAG = 16
    fileprivate let VIOLET_BUTTON_TAG = 17
    fileprivate let BLACK_BUTTON_TAG = 18
    fileprivate let WHITE_BUTTON_TAG = 19

    @IBOutlet weak var drawingArea: LinearInterpView!
    @IBOutlet weak var btnClearDrawing: UIButton!
    @IBOutlet weak var btnRed: UIButton!
    @IBOutlet weak var btnOrange: UIButton!
    @IBOutlet weak var btnYellow: UIButton!
    @IBOutlet weak var btnGreen: UIButton!
    @IBOutlet weak var btnBlue: UIButton!
    @IBOutlet weak var btnIndigo: UIButton!
    @IBOutlet weak var btnViolet: UIButton!
    @IBOutlet weak var btnBlack: UIButton!
    @IBOutlet weak var btnWhite: UIButton!
    @IBOutlet weak var brushSizeSlider: TGPDiscreteSlider!
    @IBOutlet weak var brushSizeLabels: TGPCamelLabels!

    fileprivate var colorButtons: [UIButton]!
    fileprivate let brushSizes = [2, 4, 6, 8, 10]

    @IBAction func onClearButtonClick(_ sender: AnyObject) {
        drawingArea.erase()
    }

    func onBrushSizeValueChanged(_ sender: TGPDiscreteSlider) {
        let position = Int(sender.value)
        let size = brushSizes[position]
        drawingArea.setBrushSize(Int32(size))
        brushSizeLabels.value = UInt(position)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        colorButtons = [btnRed!, btnOrange!, btnYellow!, btnGreen!, btnBlue!, btnIndigo!, btnViolet!, btnBlack!, btnWhite!]
        btnRed.backgroundColor = UIColor.colorWithRGBHex(Color.red.rawValue)
        btnRed.tag = RED_BUTTON_TAG
        btnOrange.backgroundColor = UIColor.colorWithRGBHex(Color.orange.rawValue)
        btnOrange.tag = ORANGE_BUTTON_TAG
        btnYellow.backgroundColor = UIColor.colorWithRGBHex(Color.yellow.rawValue)
        btnYellow.tag = YELLOW_BUTTON_TAG
        btnGreen.backgroundColor = UIColor.colorWithRGBHex(Color.green.rawValue)
        btnGreen.tag = GREEN_BUTTON_TAG
        btnBlue.backgroundColor = UIColor.colorWithRGBHex(Color.blue.rawValue)
        btnBlue.tag = BLUE_BUTTON_TAG
        btnIndigo.backgroundColor = UIColor.colorWithRGBHex(Color.indigo.rawValue)
        btnIndigo.tag = INDIGO_BUTTON_TAG
        btnViolet.backgroundColor = UIColor.colorWithRGBHex(Color.violet.rawValue)
        btnViolet.tag = VIOLET_BUTTON_TAG
        btnBlack.backgroundColor = UIColor.colorWithRGBHex(Color.black.rawValue)
        btnBlack.tag = BLACK_BUTTON_TAG
        btnWhite.backgroundColor = UIColor.colorWithRGBHex(Color.white.rawValue)
        btnWhite.tag = WHITE_BUTTON_TAG

        // Bind color palette buttons to an action method
        for button in colorButtons {
            button.addTarget(self, action: #selector(onColorButtonClick(_:)), for: .touchDown)
        }

        // Make Clear button's label uppercase
        btnClearDrawing.setTitle(btnClearDrawing.currentTitle?.uppercased(), for: [])

        // Configure brush size control
        brushSizeSlider.minimumValue = 0
        brushSizeSlider.incrementValue = 1
        brushSizeSlider.tickCount = Int32(brushSizes.count)
        brushSizeSlider.addTarget(self, action: #selector(onBrushSizeValueChanged(_:)), for: UIControlEvents.valueChanged)

        var names = [String]()
        for s in brushSizes {
            names.append(String(s))
        }
        brushSizeLabels.names = names
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        // Make a shadow around whole view of this view controller
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 5.0
        view.layer.shadowColor = UIColor.black.cgColor;
        view.layer.shadowOffset = CGSize(width: 0, height: -1)
        view.layer.shadowOpacity = 0.6

        let shadowPath: UIBezierPath = UIBezierPath(roundedRect: view.layer.bounds, cornerRadius: CGFloat(8.0))
        view.layer.shadowPath = shadowPath.cgPath

        // Color palette buttons with rounded corners
        for button in colorButtons {
            button.layer.cornerRadius = 4
            button.clipsToBounds = true
        }

        // Labels on brush size slider
        brushSizeLabels.ticksDistance = brushSizeSlider.bounds.width / CGFloat(brushSizes.count)
    }

    func onColorButtonClick(_ button: UIButton) {
        switch button.tag {
        case RED_BUTTON_TAG:
            drawingArea.setColor(UIColor.colorWithRGBHex(Color.red.rawValue))
        case ORANGE_BUTTON_TAG:
            drawingArea.setColor(UIColor.colorWithRGBHex(Color.orange.rawValue))
        case YELLOW_BUTTON_TAG:
            drawingArea.setColor(UIColor.colorWithRGBHex(Color.yellow.rawValue))
        case GREEN_BUTTON_TAG:
            drawingArea.setColor(UIColor.colorWithRGBHex(Color.green.rawValue))
        case BLUE_BUTTON_TAG:
            drawingArea.setColor(UIColor.colorWithRGBHex(Color.blue.rawValue))
        case INDIGO_BUTTON_TAG:
            drawingArea.setColor(UIColor.colorWithRGBHex(Color.indigo.rawValue))
        case VIOLET_BUTTON_TAG:
            drawingArea.setColor(UIColor.colorWithRGBHex(Color.violet.rawValue))
        case BLACK_BUTTON_TAG:
            drawingArea.setColor(UIColor.colorWithRGBHex(Color.black.rawValue))
        case WHITE_BUTTON_TAG:
            drawingArea.setColor(UIColor.colorWithRGBHex(Color.white.rawValue))
        default:
            fatalError("A color is not handled")
        }
    }
}

enum Color: Int {
    case red = 0xD10000
    case orange = 0xFF6622
    case yellow = 0xFFDA21
    case green = 0x33DD00
    case blue = 0x1133CC
    case indigo = 0x220066
    case violet = 0x330044
    case black = 0x000000
    case white = 0xFFFFFF
}
