//
//  SettingsTableViewCell.swift
//  CHISI
//
//  Created by Mykolaj on 19.10.16.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var settingNameLabel: UILabel!
    
    override final func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override final func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
