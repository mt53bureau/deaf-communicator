//
//  SettingsViewController.swift
//  CHISI
//
//  Created by Mykolaj on 14.10.16.
//

import UIKit

class SettingsViewController: UITableViewController {

    private static let settingCellId = "settingsTableViewCellId"

    private var settings = [SpeechApiSetting]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "SettingsTableViewCell", bundle: nil),
                forCellReuseIdentifier: SettingsViewController.settingCellId)
        createSettings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }


    private final func createSettings() {
        let s1 = SpeechApiSetting(SpeechRecognizerType.openEars,
                "OpenEars Speech API (offline)")
        settings.append(s1)
        // TODO Disable this option on devices lower than iOS 10
        let s3 = SpeechApiSetting(SpeechRecognizerType.appleIos10,
                "Apple iOS 10 Speech API (online only)")
        settings.append(s3)

        // Get speech engine selected by a user
        let selectedRecognizer = SettingsHelper.getSpeechRecognizerType()
        for s in settings {
            if s.speechRecognizer == selectedRecognizer {
                s.selected = true
            } else {
                s.selected = false
            }
        }
    }

    // MARK: - Table view data source

    override final func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }

    override final func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item: SpeechApiSetting = settings[indexPath.row]
        let cell: SettingsTableViewCell = tableView.dequeueReusableCell(
                withIdentifier:SettingsViewController.settingCellId, for : indexPath) as! SettingsTableViewCell
        cell.settingNameLabel.text = item.settingName

        if item.selected {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.none
        }

        return cell
    }

    override final func numberOfSections(in: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Clear all selections
        for s in settings {
            s.selected = false
        }

        let item: SpeechApiSetting = settings[indexPath.row]
        item.selected = true
        SettingsHelper.saveSpeechRecognizerType(item.speechRecognizer)

        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.checkmark
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let item: SpeechApiSetting = settings[indexPath.row]
        item.selected = false
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.none
    }

}

enum SpeechRecognizerType: Int {
    case openEars   = 1001
    case appleIos10 = 1003

    static let openEarsRecognizerCode = 1001
    static let appleIosRecognizerCode = 1003
}

class SpeechApiSetting: NSObject {
    var speechRecognizer: SpeechRecognizerType
    var settingName: String
    var selected: Bool = false
    let accessible: Bool

    init(_ recognizer: SpeechRecognizerType, _ settingName: String) {
        speechRecognizer = recognizer
        self.settingName = settingName

        if !OsVersion.isEqualOrLaterIos10() && recognizer == SpeechRecognizerType.appleIos10 {
            accessible = false
        } else {
            accessible = true
        }
    }
}
