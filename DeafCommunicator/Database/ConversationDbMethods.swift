//
// Created by A. Mykolaj on 06.10.16.
//

import Foundation
import SharkORM
import SwiftDate

class ConversationDbMethods {

    static func getTalk(_ conversationId: Int, order: SortOrder = .asc) -> Array<Phrase> {
        // FIXME BUG Use only raw SQL queries because after deleting some data from underlying table using raw query (which i'm doing a lot) those deleted records still exist in context of SharkORM
       let query = Phrase.query()
               .where("conversationId = \(conversationId)")!

               if order == .asc {
                   query.order(by: "timestamp")
               } else {
                   query.order(byDescending: "timestamp")
               }

        guard let queryResult = query.fetch() else {
            return [Phrase]()
        }

        var result = [Phrase]()
        for r in queryResult {
            result.append(r as! Phrase)
        }
        return result
    }

    static func getTalkForPatient(_ patientId: Int) -> SRKResultSet {
        // FIXME BUG Use only raw SQL queries because after deleting some data from underlying table using raw query (which i'm doing a lot) those deleted records still exist in context of SharkORM
        return Phrase.query()
                .where("patientId = \(patientId)")
                .order(byDescending: "timestamp")
                .fetch()
    }

    static func getConversations(_ forDate: Date? = nil, order: SortOrder = .desc) -> Array<ConversationWrapper> {
        // Create column aliases for SQL query
        let c_conversation_id = "c_id"
        let c_start_time = "c_start_time"
        let c_end_time = "c_end_time"
        let c_display_id = "c_display_id"
        let c_patient_id = "c_patient_id"
        let p_id = "p_id"
        let p_text = "p_text"
        let p_timestamp = "p_timestamp"
        let p_patient_id = "p_patient_id"
        let whereClausePlaceholder = "#where_clause#"

        var query = "" +
                " SELECT " +
                "       c.id AS \(c_conversation_id), " +
                "       c.startTime AS \(c_start_time), " +
                "       c.endTime AS \(c_end_time), " +
                "       c.displayId AS \(c_display_id), " +
                "       c.patientId AS \(c_patient_id), " +
                "       p.id AS \(p_id), " +
                "       p.text AS \(p_text), " +
                "       p.timestamp AS \(p_timestamp), " +
                "       p.patientId AS \(p_patient_id) " +
                " FROM conversation AS c " +
                " JOIN phrase AS p " +
                "     ON c.id = p.conversationId " +
                whereClausePlaceholder +
                " ORDER BY \(c_start_time), \(p_timestamp)"
        if order == .desc {
            query += " DESC "
        }
        query += " ;"

        // Handle Date parameter
        var whereClause = ""
        if forDate != nil {
            let unitFlags: Set<Calendar.Component> = [.year, .month, .day]
            var cmpStart = Calendar.current.dateComponents(unitFlags, from: forDate!)
            cmpStart.hour = 0
            cmpStart.minute = 0
            cmpStart.second = 0

            var cmpEnd = Calendar.current.dateComponents(unitFlags, from: forDate!)
            cmpEnd.hour = 23
            cmpEnd.minute = 59
            cmpEnd.second = 59

            let startTime = try! DateInRegion(components: cmpStart).absoluteDate.timeIntervalSince1970
            let endTime = try! DateInRegion(components: cmpEnd).absoluteDate.timeIntervalSince1970
            whereClause = " WHERE \(p_timestamp) >= \(startTime) AND \(p_timestamp) <= \(endTime)"
        }

        // Inflate a WHERE clause into a query
        let finalQuery = query.replacingOccurrences(of: whereClausePlaceholder, with: whereClause)
        debugPrint("SQL: \(finalQuery)")

        // Conversation ID to conversation object mapping
        var conversationWrappers = [Int : ConversationWrapper]()

        let result: SRKRawResults = SharkORM.rawQuery(finalQuery)
        for i in 0..<result.rawResults.count {
            let row = result.rawResults[i]

            // A row from a query result
            let currentRecord = row as! Dictionary<String, AnyObject>

            let cPatientId = currentRecord[c_patient_id] as! String
            let pPatientId = currentRecord[p_patient_id] as! String
            precondition(cPatientId == pPatientId, "SQL query works wrong. Patient ids from both tables must be identical")

            let conversationId = currentRecord[c_conversation_id] as! NSNumber
            // Construct an object representing a conversation
            let conversation = Conversation()
            conversation.id = conversationId
            conversation.patientId = cPatientId
            if let st = currentRecord[c_start_time] {
                conversation.startTime = st as! NSNumber
            } else {
                preconditionFailure("Conversation start time can not be null")
            }
            if let et = currentRecord[c_end_time] {
                conversation.endTime = et as? NSNumber
            }
            conversation.displayId = currentRecord[c_display_id] as! NSNumber

            // Create phrase object and add it to a conversation wrapper
            let phrase = Phrase()
            phrase.id =  currentRecord[p_id] as! NSNumber
            phrase.text = currentRecord[p_text] as! String
            phrase.timestamp = currentRecord[p_timestamp] as! NSNumber
            phrase.conversationId = conversationId
            phrase.patientId = pPatientId
            
            if let existingConversation = conversationWrappers[Int(conversationId)] {
                // Add a phrase to an existing wrapper object
                existingConversation.addPhrase(phrase)
            } else {
                // Create conversation wrapper object
                let wrapper = ConversationWrapper(conversation)

                // Add a phrase to a wrapper object
                wrapper.addPhrase(phrase)

                // Put a wrapper object into a mapping
                conversationWrappers.updateValue(wrapper, forKey: wrapper.conversationId)
            }
        }
        var a = Array(conversationWrappers.values)
        a.sort {
            $0.startTime.isAfter(date:$1.startTime, granularity: Calendar.Component.second)
        }
        return a
    }

    /// Returns a date of the oldest conversation existing.
    /// Or null in case there aren't any conversations in a database.
    static func getOldestConversationDate() -> Date? {
        let resultSet = Phrase.query().limit(1).order(by: "timestamp").fetch()
        guard let result = resultSet else {
            return nil
        }
        if result.count == 0 {
            return nil
        }
        let c = result[0] as! Phrase
        return Date(timeIntervalSince1970: Double(c.timestamp))
    }

    static func getMaxConversationId() -> Int {
        // Column aliases for SQL query
        let p_conversation_id = "c_id"

        let query = "" +
        "" +
        " SELECT " +
        "       id AS \(p_conversation_id) " +
        " FROM conversation " +
        " ORDER BY \(p_conversation_id) DESC " +
        " LIMIT 1 ;"

        let result: SRKRawResults = SharkORM.rawQuery(query)
        if result.rawResults.count == 0 {
            return 0
        } else {
            let row = result.rawResults[0]
            let currentRecord = row as! Dictionary<String, AnyObject>
            return (currentRecord[p_conversation_id] as! Int)
        }
    }
    
    static func getLongestPatientId() -> String? {
        // Column aliases for SQL query
        let p_patient_id = "p_id"
        
        let query = "" +
        "" +
        " SELECT " +
        "       patientId AS \(p_patient_id) " +
        " FROM conversation " +
        " ORDER BY length(\(p_patient_id)) DESC " +
        " LIMIT 1 ;"
        
        let result: SRKRawResults = SharkORM.rawQuery(query)
        if result.rawResults.count == 0 {
            return nil
        } else {
            let row = result.rawResults[0]
            let currentRecord = row as! Dictionary<String, AnyObject>
            return (currentRecord[p_patient_id] as! String)
        }
    }

    static func removeConversation(_ conversationId: Int) -> Bool {
        let query = "" +
                " DELETE " +
                " FROM conversation " +
                " WHERE id = \(conversationId) ;"
        let result: SRKRawResults = SharkORM.rawQuery(query)
        if let error = result.error {
            debugPrint("Error deleting a conversation. Message: \(error.errorMessage). (Query: '\(error.sqlQuery)')")
            return false
        } else {
            return true
        }
    }

    static func removePhrase(_ phraseId: Int) -> Bool {
        let query = "" +
                " DELETE " +
                " FROM phrase " +
                " WHERE id = \(phraseId) ;"
        let result: SRKRawResults = SharkORM.rawQuery(query)
        if let error = result.error {
            debugPrint("Error deleting a conversation. Message: \(error.errorMessage). (Query: '\(error.sqlQuery)')")
            return false
        } else {
            return true
        }
    }

    static func getConversationMaxDisplayId() -> Int? {
        // Column aliases for SQL query
        let d_id = "d_id"

        let query = "" +
                "" +
                " SELECT " +
                "       displayId AS \(d_id) " +
                " FROM conversation " +
                " ORDER BY \(d_id) DESC " +
                " LIMIT 1 ;"

        let result: SRKRawResults = SharkORM.rawQuery(query)
        if result.rawResults.count == 0 {
            return nil
        } else {
            let row = result.rawResults[0]
            let currentRecord = row as! Dictionary<String, AnyObject>
            return currentRecord[d_id] as! Int
        }
    }

    static func updateConversationDisplayId(_ conversationId: Int, newDisplayId: Int) -> Bool {
        let query = "" +
                " UPDATE conversation" +
                "       SET displayId = \(newDisplayId) WHERE id = \(conversationId) ;"

        let result: SRKRawResults = SharkORM.rawQuery(query)
        debugPrint("SQL: \(query)")
        if let error = result.error {
            debugPrint("Error updating a conversation. Message: \(error.errorMessage). (Query: '\(error.sqlQuery)')")
            return false
        } else {
            return true
        }
    }

}
