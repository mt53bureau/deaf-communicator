//
// Created by A. Mykolaj on 12.10.16.
//

import Foundation
import Speech
import SwiftDate

class AppleNativeSpeechProcessor: SpeechProcessorImpl {

    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    private let speechRecognizer: SFSpeechRecognizer!
    private var audioInputNode: AVAudioInputNode!

    override init() {
        guard let r = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US")) else {
            fatalError("Can not initialize SFSpeechRecognizer")
        }
        speechRecognizer = r
        super.init()
        speechRecognizer.delegate = self
        setupEngine()
    }

    override final var speechRecognizerType: SpeechRecognizerType {
        return SpeechRecognizerType.appleIos10
    }

    private func setupEngine() {
        guard let inputNode = audioEngine.inputNode else {
            let msg = "Error: Audio engine has no input node"
            reportStatusUpdate(msg)
            fatalError(msg)
            return
        }
        audioInputNode = inputNode
    }

    private func createRecognitionRequest() -> SFSpeechAudioBufferRecognitionRequest {
        let r = SFSpeechAudioBufferRecognitionRequest()
        // Configure request so that results are returned before audio recording is finished
        r.shouldReportPartialResults = true
        return r
    }

    override final func startListening(_ conversation: Conversation) -> Bool {
        currentConversation = conversation
        currentText = ""

        if !OsVersion.isEqualOrLaterIos10() {
            let s = "Apple Speech API is not available on a current iOS version. Please update to iOS 10 or later."
            reportStatusUpdate(s)
            currentText = s
            reportFinalSuggestion(s)
            return false
        }

        // Cancel any previous tasks if they exist
        if let recognitionTask = self.recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }

        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryRecord)
            try audioSession.setCategory(AVAudioSessionCategoryRecord)
            try audioSession.setMode(AVAudioSessionModeMeasurement)
            try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        } catch {
            let s = "Failed to set AudioSession properties: \(error)"
            print(s)
            reportStatusUpdate(s)
            return false
        }

        let request = createRecognitionRequest()
        recognitionRequest = request

        // A recognition task represents a speech recognition session.
        // We keep a reference to the task so that it can be cancelled.
        recognitionTask = speechRecognizer.recognitionTask(with: request, resultHandler: { (result, error) in
            var isFinal = false
            var recognizedText: String = ""

            if let result = result {
                recognizedText = result.bestTranscription.formattedString
                print("You said: '\(recognizedText)'")
                self.currentText = recognizedText
                isFinal = result.isFinal
                if !isFinal {
                    self.reportSuggestionUpdate(recognizedText)
                }
            }

            if error != nil || isFinal {
                request.endAudio()
                self.reportFinalSuggestion(recognizedText)
            }
        })

        let recordingFormat = audioInputNode.outputFormat(forBus: 0)
        audioInputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }

        audioEngine.prepare()

        do {
            try audioEngine.start()
        } catch {
            let msg = "Failed to start AudioEngine: \(error)"
            print(msg)
            reportStatusUpdate(msg)
            return false
        }

        reportStatusUpdate("Listening")
        return true
    }

    override final func stopListening() {
        recognitionRequest?.endAudio()
        audioInputNode?.removeTap(onBus: 0)
        audioEngine.stop()
        reportStatusUpdate("Idle")
    }

    override final func requestAuthorization(_ controller: UIViewController) {
        if .authorized == SFSpeechRecognizer.authorizationStatus() {
            return
        }

        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            var msg: String!
            switch authStatus {
            case .authorized:
                msg = "User authorized access to speech recognition"
            case .denied:
                msg = "User denied access to speech recognition"
                self.openSettings(controller)
            case .restricted:
                msg = "Speech recognition restricted on this device"
            case .notDetermined:
                msg = "Speech recognition not yet authorized"
            default:
                msg = "null"
            }

            print(msg)

            DispatchQueue.main.async {
                self.reportStatusUpdate(msg)
            }
        }
    }
}

// MARK: - SFSpeechRecognizerDelegate

extension AppleNativeSpeechProcessor : SFSpeechRecognizerDelegate {

    final func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        let msg = "Speech recognizer is now \(available ? "available" : "not available")"
        print(msg)
        reportStatusUpdate(msg)
    }
}
