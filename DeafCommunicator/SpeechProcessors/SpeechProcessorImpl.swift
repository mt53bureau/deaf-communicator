//
// Created by Mykolaj on 13.10.16.
//

import Foundation

class SpeechProcessorImpl: NSObject, SpeechProcessor {

    var currentConversation: Conversation!
    var currentText: String?
    var delegate: SpeechProcessorDelegate?
    var isStarted = false

    var speechRecognizerType: SpeechRecognizerType {
        preconditionFailure("You must override speechRecognizerType property in subclasses")
    }

    func startListening(_ conversation: Conversation) -> Bool {
        isStarted = false
        return false
    }

    func stopListening() {
        isStarted = false
    }

    func requestAuthorization(_ controller: UIViewController) {
    }

    func openSettings(_ controller: UIViewController) {
        let alertController = UIAlertController(title: "Permission Required",
                message: "The microphone permission was not authorized. Please enable it in Settings to continue.",
                preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in

            // This will open a settings screen where a user can grant a permission
            if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(appSettings as URL)
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        controller.present(alertController, animated: true, completion: nil)
    }

    func checkMicrophonePermissions() -> Bool {
        return AVAudioSession.sharedInstance().recordPermission() == .granted
    }

    func reportStatusUpdate(_ text:String) {
        delegate?.onStatusUpdate(text: text)
    }

    func reportSuggestionUpdate(_ text:String) {
        delegate?.onSuggestionUpdate(text: text)
    }

    func reportFinalSuggestion(_ text:String) {
        delegate?.onFinalSuggestion(text: text)
    }

}

protocol SpeechProcessorDelegate {
    func onSuggestionUpdate(text: String)
    func onFinalSuggestion(text: String)
    func onStatusUpdate(text: String)
}