//
// Created by tony on 13.10.16.
// Copyright (c) 2016 mt53bureau. All rights reserved.
//

import Foundation

protocol SpeechProcessor {
    var speechRecognizerType: SpeechRecognizerType { get }
    func startListening(_ conversation: Conversation) -> Bool
    func stopListening()
}
