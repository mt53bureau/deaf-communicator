//
// Created by A. Mykolaj on 12.10.16.
//

import Foundation
import SwiftDate

class OpenEarsSpeechProcessor: SpeechProcessorImpl, OEEventsObserverDelegate {

    private let logTag = "OpenEarsSpeechProcessor"
    private var openEarsCommonDictionary = [String]()
    private let dictionaryFileName = "english-common-dictionary"
    private let dictionaryFileType = "txt"
    private let openEarsEventsObserver = OEEventsObserver()
    private var langModelPath: String!
    private var dictionaryPath: String!

    override init() {
        super.init()
        prepareDictionary()
        setupOpenEars()
    }

    override var speechRecognizerType: SpeechRecognizerType {
        return SpeechRecognizerType.openEars
    }

    private func setupOpenEars() {
        self.openEarsEventsObserver.delegate = self
        let lmGenerator: OELanguageModelGenerator = OELanguageModelGenerator()
        let dictName = "GoogleEnglishDictionary"
        let error = lmGenerator.generateLanguageModel(from: openEarsCommonDictionary,
                withFilesNamed: dictName,
                forAcousticModelAtPath: OEAcousticModel.path(toModel: "AcousticModelEnglish"))

        if error != nil {
            let msg = "Language model generate error: \(error)"
            print(msg)
            reportStatusUpdate(msg)
        }

        langModelPath = lmGenerator.pathToSuccessfullyGeneratedLanguageModel(withRequestedName: dictName)
        dictionaryPath = lmGenerator.pathToSuccessfullyGeneratedDictionary(withRequestedName: dictName)
    }

    private func prepareDictionary() {
        // Fill a word dictionary to work with
        guard let path = Bundle.main.path(forResource: dictionaryFileName, ofType: dictionaryFileType) else {
            print("\(dictionaryFileName).\(dictionaryFileType) file not found")
            return
        }

        if let reader = StreamReader(path: path) {
            while let line = reader.nextLine() {
                openEarsCommonDictionary.append(line)
            }
            reader.close()
        } else {
            print("cannot open file")
        }
        print("Language dictionary size is \(openEarsCommonDictionary.count) words")
    }

    override func startListening(_ conversation: Conversation) -> Bool {
        currentText = ""
        reportStatusUpdate("")
        currentConversation = conversation

        if !checkMicrophonePermissions() {
            print("\(logTag): microphone permission not granted.")
            return false
        }

        do {
            try OEPocketsphinxController.sharedInstance().setActive(true)
        } catch {
            let msg = "Failed to activate \(type(of: OEPocketsphinxController.self)): \(error)"
            print(msg)
            reportStatusUpdate(msg)
            return false
        }
        OEPocketsphinxController.sharedInstance().startListeningWithLanguageModel(atPath: langModelPath,
                        dictionaryAtPath: dictionaryPath,
                        acousticModelAtPath: OEAcousticModel.path(toModel: "AcousticModelEnglish"),
                        languageModelIsJSGF: false)
        isStarted = true
        return true
    }

    override func stopListening() {
        if let oe = OEPocketsphinxController.sharedInstance() {
            if oe.isListening {
                oe.stopListening()
            }
        }
        if isStarted {
            if let s = currentText {
                reportFinalSuggestion(s)
            } else {
                reportFinalSuggestion("")
            }
        }
        isStarted = false
    }

    override func requestAuthorization(_ controller: UIViewController) {
        let status:AVAudioSessionRecordPermission = AVAudioSession.sharedInstance().recordPermission()
        if AVAudioSessionRecordPermission.granted == status {
            print("\(logTag): Microphone permission has been granted already")
            return
        }

        switch (status) {
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted: Bool) -> Void in
                if granted {
                    print("\(self.logTag): Microphone permissions granted")
                } else {
                    print("\(self.logTag): Microphone permissions not granted")
                }
            })

        case AVAudioSessionRecordPermission.denied:
            openSettings(controller)
            break

        case AVAudioSessionRecordPermission.granted:
            print("\(self.logTag): Microphone permissions checked and granted")
            break

        default:
            fatalError("A Switch statement does not include all cases")
            break
        }
    }

    // MARK: - OEEventsObserverDelegate
    func pocketsphinxDidStartListening() {
        let s = "OpenEars is now listening."
        print(s)
        reportStatusUpdate(s)
    }

    func pocketsphinxDidDetectSpeech() {
        let s = "OpenEars has detected speech."
        print(s)
        reportStatusUpdate(s)
    }

    func pocketsphinxDidDetectFinishedSpeech() {
        let s = "OpenEars has detected a period of silence, concluding an utterance."
        print(s)
        reportStatusUpdate(s)
    }

    func pocketsphinxDidStopListening() {
        let s = "OpenEars has stopped listening."
        print(s)
        reportStatusUpdate(s)
    }

    func pocketsphinxDidSuspendRecognition() {
        let s = "OpenEars has suspended recognition."
        print(s)
        reportStatusUpdate(s)
    }

    func pocketsphinxDidResumeRecognition() {
        let s = "OpenEars has resumed recognition."
        print(s)
        reportStatusUpdate(s)
    }

    @objc(pocketsphinxDidChangeLanguageModelToFile:andDictionary:) func pocketsphinxDidChangeLanguageModel(toFile: String, andDictionary: String) {
        let s = "OpenEars is now using the following language model: \(toFile) and the following dictionary: \(andDictionary)"
        print(s)
        reportStatusUpdate(s)
    }

    func pocketSphinxContinuousSetupDidFail(withReason reasonForFailure: String) {
        let s = "Listening setup wasn't successful and returned the failure reason: \(reasonForFailure)"
        print(s)
        reportStatusUpdate(s)
    }

    func pocketSphinxContinuousTeardownDidFail(withReason reasonForFailure: String) {
        let s = "Listening teardown wasn't successful and returned the failure reason: \(reasonForFailure)"
        print(s)
        reportStatusUpdate(s)
    }

    func pocketsphinxDidReceiveHypothesis(_ hypothesis: String!, recognitionScore: String!, utteranceID: String!) {
        let score: String = recognitionScore != nil ? recognitionScore! : "?"
        let utterId: String = utteranceID != nil ? utteranceID : "?"

        guard let recognizedText = hypothesis else {
            print("Empty hypothesis received with a score of '\(score)'" +
                    " and utteranceId '\(utterId)'")
            return
        }

        if recognizedText.isEmpty {
            currentText = ""
            reportSuggestionUpdate(currentText!)
            return
        }

        var normalizedText = ""
        let words: [String] = recognizedText.components(separatedBy: " ")

        if words.count > 1 {
            for (index, word) in words.enumerated() {
                if index == 0 {
                    normalizedText += word.localizedCapitalized
                } else {
                    normalizedText += " \(word.localizedLowercase)"
                }
            }
        } else {
            normalizedText = words[0].localizedCapitalized
        }

        let s = "The received hypothesis is '\(normalizedText)' with a score of '\(score)' and an utteranceID '\(utterId)'"
        debugPrint(s)

        if let existingText = currentText {
            if !existingText.isEmpty {
                currentText = existingText + " " + normalizedText.localizedLowercase
            } else {
                currentText = normalizedText
            }
        } else {
            currentText = normalizedText
        }

        guard let text = currentText else {
            fatalError("No way you should allow this to happen for the text to be null.")
        }

        reportSuggestionUpdate(text)
    }

}
