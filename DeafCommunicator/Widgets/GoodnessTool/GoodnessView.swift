//
//  GoodnessView.swift
//  CHISI
//
//  Created by Mykolaj on 24.11.16.
//

import UIKit

class GoodnessView: UIView {

    fileprivate static let BUTTON_WIDTH: CGFloat = 48.0
    fileprivate static let BUTTON_HEIGHT: CGFloat = 48.0
    fileprivate static let RULER_THICKNESS: CGFloat = 24.0
    fileprivate static let RULER_LABEL_HEIGHT: CGFloat = 20.0
    fileprivate static let BUTTON_LABEL_HEIGHT: CGFloat = 34.0
    fileprivate static let VERTICAL_SPACING: CGFloat = 1.0
    fileprivate static let GRADIENT_LINE_WIDTH: CGFloat = 10.0
    fileprivate static let RULER_GRADIENT_COLORS = [UIColor.colorWithRGBHex(0x308a45).cgColor,
                                                    UIColor.colorWithRGBHex(0x90bd31).cgColor,
                                                    UIColor.colorWithRGBHex(0xf3f320).cgColor,
                                                    UIColor.colorWithRGBHex(0xf2b10f).cgColor,
                                                    UIColor.colorWithRGBHex(0xf37100).cgColor,
                                                    UIColor.colorWithRGBHex(0xd30100).cgColor]
    fileprivate static let BACKGROUND_COLOR = UIColor.white
    fileprivate let textColor: UIColor = UIColor.darkGray
    fileprivate var labelFont: UIFont!
    fileprivate let paddingTop: CGFloat = 0.0

    fileprivate var rulerLabelY: CGFloat {
        get {
            return paddingTop
        }
        set(value) {
            // This property should not be settable
        }
    }

    fileprivate var rulerCanvasY: CGFloat {
        get {
            return rulerLabelY + GoodnessView.RULER_LABEL_HEIGHT + GoodnessView.VERTICAL_SPACING
        }
        set(value) {
            // This property should not be settable
        }
    }

    fileprivate var buttonLabelY: CGFloat {
        get {
            return rulerCanvasY + GoodnessView.RULER_THICKNESS
        }
        set(value) {
            // This property should not be settable
        }
    }

    fileprivate var buttonY: CGFloat {
        get {
            return buttonLabelY + GoodnessView.BUTTON_LABEL_HEIGHT
        }
        set(value) {
            // This property should not be settable
        }
    }

    fileprivate var bottomRulerLabelY: CGFloat {
        get {
            return buttonY + GoodnessView.BUTTON_HEIGHT
        }
        set(value) {
            // This property should not be settable
        }
    }

    fileprivate var buttons = [GoodnessLevel: UIButton]()
    fileprivate var buttonLabels = [GoodnessLevel: UILabel]()
    fileprivate var bottomRulerLabels = [GoodnessLevel: UILabel]()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        if let f = UIFont(name: "Helvetica Neue", size: 18.0) {
            labelFont = f
        } else {
            labelFont = UIFont.systemFont(ofSize: 18.0)
        }

        isUserInteractionEnabled = true
        backgroundColor = GoodnessView.BACKGROUND_COLOR

        var button = UIButton(frame: CGRect(x: 0, y: 0, width: GoodnessView.BUTTON_WIDTH, height: GoodnessView.BUTTON_HEIGHT))
        button.setImage(UIImage(named: "ic_goodness_level1_48pt.png"), for: [])
        buttons[GoodnessLevel.none] = button

        button = UIButton(frame: CGRect(x: 0, y: 0, width: GoodnessView.BUTTON_WIDTH, height: GoodnessView.BUTTON_HEIGHT))
        button.setImage(UIImage(named: "ic_goodness_level2_48pt.png"), for: [])
        buttons[GoodnessLevel.mild] = button

        button = UIButton(frame: CGRect(x: 0, y: 0, width: GoodnessView.BUTTON_WIDTH, height: GoodnessView.BUTTON_HEIGHT))
        button.setImage(UIImage(named: "ic_goodness_level3_48pt.png"), for: [])
        buttons[GoodnessLevel.moderate] = button

        button = UIButton(frame: CGRect(x: 0, y: 0, width: GoodnessView.BUTTON_WIDTH, height: GoodnessView.BUTTON_HEIGHT))
        button.setImage(UIImage(named: "ic_goodness_level4_48pt.png"), for: [])
        buttons[GoodnessLevel.severe] = button

        button = UIButton(frame: CGRect(x: 0, y: 0, width: GoodnessView.BUTTON_WIDTH, height: GoodnessView.BUTTON_HEIGHT))
        button.setImage(UIImage(named: "ic_goodness_level5_48pt.png"), for: [])
        buttons[GoodnessLevel.verySevere] = button

        button = UIButton(frame: CGRect(x: 0, y: 0, width: GoodnessView.BUTTON_WIDTH, height: GoodnessView.BUTTON_HEIGHT))
        button.setImage(UIImage(named: "ic_goodness_level6_48pt.png"), for: [])
        buttons[GoodnessLevel.max] = button

        for (level, button) in buttons {
            button.addTarget(self, action: #selector(ratingButtonTapped(_:)), for: .touchDown)
            addSubview(button)
        }

        // Labels above buttons
        let labelWidth = GoodnessView.BUTTON_WIDTH * 1.5
        let goodnessLevels:[GoodnessLevel] = [.none, .mild, .moderate, .severe, .verySevere, .max]

        for level in goodnessLevels {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: labelWidth, height: GoodnessView.BUTTON_LABEL_HEIGHT))
            label.font = UIFont.systemFont(ofSize: 13.0)
            label.textAlignment = NSTextAlignment.center
            label.numberOfLines = 0
            label.text = level.humanDescription()
            addSubview(label)
            buttonLabels[level] = label
        }

        assert(buttonLabels.count == buttons.count, "Number of button labels should be the same as number of buttons")

        // Labels at the bottom
        var label = UILabel(frame: CGRect(x: 0, y: 0, width: labelWidth, height: GoodnessView.BUTTON_LABEL_HEIGHT))
        label.text = "0"
        bottomRulerLabels[GoodnessLevel.none] = label
        label = UILabel(frame: CGRect(x: 0, y: 0, width: labelWidth, height: GoodnessView.BUTTON_LABEL_HEIGHT))
        label.text = "1-3"
        bottomRulerLabels[GoodnessLevel.mild] = label
        label = UILabel(frame: CGRect(x: 0, y: 0, width: labelWidth, height: GoodnessView.BUTTON_LABEL_HEIGHT))
        label.text = "4-6"
        bottomRulerLabels[GoodnessLevel.moderate] = label
        label = UILabel(frame: CGRect(x: 0, y: 0, width: labelWidth, height: GoodnessView.BUTTON_LABEL_HEIGHT))
        label.text = "7-9"
        bottomRulerLabels[GoodnessLevel.verySevere] = label
        label = UILabel(frame: CGRect(x: 0, y: 0, width: labelWidth, height: GoodnessView.BUTTON_LABEL_HEIGHT))
        label.text = "10"
        bottomRulerLabels[GoodnessLevel.max] = label

        for (_, label) in bottomRulerLabels {
            label.font = self.labelFont
            label.textAlignment = NSTextAlignment.center
            label.numberOfLines = 1
            addSubview(label)
        }

    }

//    override var intrinsicContentSize: CGSize {
//        let btnCount = buttons.count
//        let width = btnCount * Int(GoodnessView.BUTTON_WIDTH) + btnCount * (Int(GoodnessView.BUTTON_WIDTH) + 5)
//        return CGSize(width: width, height: 128)
//    }

    override func layoutSubviews() {
        layoutButtons()
        layoutButtonLabels()
        layoutBottomRulerLabels()
    }

    fileprivate func layoutButtons() {
        let halfButtonWidth = Float(GoodnessView.BUTTON_WIDTH / 2)

        // Color ruler should begin right above the center of the first button and it should end above the center of the
        // last button. So we substract
        let frameWidth = Float(frame.size.width)
        var buttonFrame = CGRect(x: 0, y: 0, width: GoodnessView.BUTTON_WIDTH, height: GoodnessView.BUTTON_HEIGHT)

        // How much space do we have between the centers of two neighbour buttons
        let buttonSpacingHorizontal = Float(frameWidth - halfButtonWidth * 2) / Float(buttons.count - 1)

        // Offset each button's origin by the length of the button plus spacing.
        for (level, button) in buttons {
            var index = 0
            switch level {
            case .none:
                index = 0
            case .mild:
                index = 1
            case .moderate:
                index = 2
            case .severe:
                index = 3
            case .verySevere:
                index = 4
            case .max:
                index = 5
            }
            buttonFrame.origin.x = CGFloat(Float(index) * buttonSpacingHorizontal)
            buttonFrame.origin.y = buttonY
            button.frame = buttonFrame
        }
    }

    fileprivate func layoutButtonLabels() {
        precondition(buttonLabels.count == buttons.count, "Number of button labels should be the same as number of buttons")
        for (level, label) in buttonLabels {
            label.frame.origin.y = buttonLabelY
            guard let correspondingButton = buttons[level] else {
                continue
            }

            label.center.x = correspondingButton.center.x
            if level == .max {
                let diff = self.frame.width - (label.frame.origin.x + label.frame.width)
                if diff < 0 {
                    label.frame.origin.x = label.frame.origin.x - diff * -1
                }
            }
        }
    }

    fileprivate func layoutBottomRulerLabels() {
        // Align bottom labels based on buttons alignment
        for (level, label) in bottomRulerLabels {
            label.frame.origin.y = bottomRulerLabelY

            // Place a middle label at the middle of the screen, and place other labels right below of a
            // corresponding button
            if level == .moderate {
                label.center.x = self.frame.width / 2
            } else {
                if let button: UIButton = buttons[level] {
                    label.center.x = button.center.x
                }
            }
        }
    }

    override func draw(_ rect: CGRect) {
        guard let context: CGContext = UIGraphicsGetCurrentContext() else {
            return
        }

        drawRuler(context)
    }

    fileprivate func drawRuler(_ context: CGContext) {
        let rect = addRulerCanvas(context)
        drawRulerLabels(context, rulerCanvas: rect)
        drawRulerMarks(context, rulerCanvas: rect)
    }

    fileprivate func addRulerCanvas(_ context: CGContext) -> CGRect {
        context.saveGState()
        let halfButtonWidth = Float(GoodnessView.BUTTON_WIDTH / 2)
        let halfLineThickness = Float(GoodnessView.GRADIENT_LINE_WIDTH / 2)

        // Color ruler should begin above the center of very first button and it should end above the center of a
        // last button. So we substract
        // Set size for a color ruler.
        let frameWidth = Float(frame.size.width)

        let canvasX = halfButtonWidth - halfLineThickness

        let rulerRect = CGRect(x: CGFloat(canvasX),
                y: rulerCanvasY,
                width: CGFloat(frameWidth - halfButtonWidth * 2 + halfLineThickness * 2),
                height: GoodnessView.RULER_THICKNESS)

        // Locations of centers of buttons on the view
        var gradientKeyColorLocations = [CGFloat]()
        let buttonCount = buttons.count
        let gradientStep = Float(1.0) / Float(buttonCount - 1)

        for i in 0 ..< buttonCount {
            gradientKeyColorLocations.append(CGFloat(Float(i) * gradientStep))
        }

        // Create a gradient
        let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceRGB()
        if let gradient: CGGradient = CGGradient(colorsSpace: colorSpace, colors: GoodnessView.RULER_GRADIENT_COLORS as CFArray, locations: gradientKeyColorLocations) {
            // Draw gradient

            context.addRect(rulerRect)
            context.clip()
            let startPoint = CGPoint(x: rulerRect.minX, y: rulerRect.midY)
            let endPoint = CGPoint(x: rulerRect.maxX, y: rulerRect.midY)
            context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: CGGradientDrawingOptions(rawValue: 0))
        }
        context.restoreGState()
        return rulerRect
    }

    fileprivate func drawRulerMarks(_ context: CGContext, rulerCanvas: CGRect) {
        context.saveGState()
        context.setFillColor(GoodnessView.BACKGROUND_COLOR.cgColor)

        let rulerPadding = Float(rulerCanvas.origin.x - frame.origin.x)

        let checkmarkCount = 10

        // Distance between centers of two neighbour check marks along X axis
        let checkmarkDistance = Float(rulerCanvas.size.width) / Float(checkmarkCount)

        let halfLineWidth = Float(GoodnessView.GRADIENT_LINE_WIDTH / 2)

        var checkmarkLocations = [Float]()

        for i in 0 ..< checkmarkCount {
            checkmarkLocations.append(Float(i) * checkmarkDistance)
        }

        let y = Float(rulerCanvas.origin.y + GoodnessView.GRADIENT_LINE_WIDTH)
        for (i, location) in checkmarkLocations.enumerated() {
            let pointX = location + rulerPadding + halfLineWidth
            var w: Float

            // Make the last rectangle with smaller width than others
            if i == checkmarkLocations.count - 1 {
                w = checkmarkDistance - halfLineWidth * 2
            } else {
                w = checkmarkDistance - halfLineWidth
            }

            let rectangle = CGRect(x: CGFloat(pointX),
                    y: CGFloat(y),
                    width: CGFloat(w),
                    height: GoodnessView.RULER_THICKNESS - GoodnessView.GRADIENT_LINE_WIDTH)
            context.fill(rectangle)
        }
        context.restoreGState()
    }

    /// This function draws the labels from 0 to 10 above a ruler.
    fileprivate func drawRulerLabels(_ context: CGContext, rulerCanvas: CGRect) {
        let textAttributes: [String: Any] = [NSForegroundColorAttributeName: textColor,
                                             NSFontAttributeName: labelFont]

        let labelCount: Int = 10
        let spacing: Float = Float(rulerCanvas.size.width) / Float(labelCount)

        for i in 0 ... labelCount {
            let label: String = "\(i)"

            // Find a width required to draw a string
            let textSize: CGSize = label.size(attributes: textAttributes)
            let posX = spacing * Float(i) + Float(rulerCanvas.origin.x)

            // Draw a label and shift it's position by half of the label's width to the left
            let textArea = CGRect(x: CGFloat(posX - Float(textSize.width) / 2),
                    y: rulerLabelY,
                    width: textSize.width,
                    height: textSize.height)
            label.draw(in: textArea, withAttributes: textAttributes)

#if NOTHING
            context.saveGState()
            context.setStrokeColor(UIColor.red.cgColor)
            context.stroke(textArea, width: 1.0)
            context.restoreGState()
#endif
        }
    }

    fileprivate func drawButtonLabels(_ context: CGContext, rulerCanvas: CGRect) {
        let labelWidth: Float = Float(GoodnessView.BUTTON_WIDTH) * 1.5
        let goodnessLevels:[GoodnessLevel] = [.none, .mild, .moderate, .severe, .verySevere, .max]
        let font = UIFont.systemFont(ofSize: 13.0)

        let attributes: [String: Any] = [NSForegroundColorAttributeName: textColor,
                                         NSFontAttributeName: font]

        let spacing: Float = Float(bounds.size.width) / Float(goodnessLevels.count)

        for (i, level) in goodnessLevels.enumerated() {
            let label: String = level.humanDescription()
            let labelX: Float = spacing * Float(i)
            let textSize: CGSize = label.size(attributes: attributes)
            let textWidth: Float = Float(textSize.width)

            if textWidth <= labelWidth {
                // Deal with single line text
                let widthDifference = (labelWidth - textWidth) / 2
                let centeredX = CGFloat(labelX + widthDifference)
                let centeredRect = CGRect(x: centeredX, y: buttonLabelY + (GoodnessView.BUTTON_LABEL_HEIGHT - textSize.height) / 2.0 , width: CGFloat(textWidth), height: textSize.height)
                label.draw(in: centeredRect, withAttributes: attributes)

#if DEBUG
                context.saveGState()
                context.setStrokeColor(UIColor.red.cgColor)
                context.stroke(centeredRect, width: 1.0)
                context.restoreGState()
#endif
            } else {
                // Deal with multi-line text
                let r = CGRect(x: CGFloat(labelX), y: buttonLabelY, width: CGFloat(labelWidth), height: textSize.height * 2)
                label.draw(in: r, withAttributes: attributes)

#if DEBUG
                context.saveGState()
                context.setStrokeColor(UIColor.red.cgColor)
                context.stroke(r, width: 1.0)
                context.restoreGState()
#endif
            }

#if DEBUG
            context.saveGState()
            context.setStrokeColor(UIColor.blue.cgColor)
            context.stroke(CGRect(x: CGFloat(labelX), y: buttonLabelY, width: CGFloat(labelWidth), height: GoodnessView.BUTTON_LABEL_HEIGHT))
            context.restoreGState()
#endif
        }
    }

    func ratingButtonTapped(_ button: AnyObject) {

    }

}

enum GoodnessLevel: Int {
    case none = 0
    case mild = 1
    case moderate = 2
    case severe = 3
    case verySevere = 4
    case max = 5

    private static let CASE_COUNT = 6
    static func count() -> Int {
        return GoodnessLevel.CASE_COUNT
    }

    func humanDescription() -> String {
        switch self {
        case .none:
            return "No Pain"
        case .mild:
            return "Mild"
        case .moderate:
            return "Moderate"
        case .severe:
            return "Severe"
        case .verySevere:
            return "Very Severe"
        case .max:
            return "Worst Pain Possible"
        default:
            return ""
        }
    }

}
