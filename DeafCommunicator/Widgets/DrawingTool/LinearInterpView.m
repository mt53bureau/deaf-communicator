#import "LinearInterpView.h"

@implementation LinearInterpView {
    UIBezierPath *currentPath;
    UIColor *currentColor;
    NSMutableArray *pathArray;
    NSMutableArray *colorArray;
    int brushSize;
}

const int DEFAULT_LINE_WIDTH = 2;
const int MAX_BRUSH_WIDTH = 10;
const int MIN_BRUSH_WIDTH = 2;

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setMultipleTouchEnabled:NO];
        [self setBackgroundColor:[UIColor whiteColor]];
        brushSize = DEFAULT_LINE_WIDTH;
        pathArray = [[NSMutableArray alloc] init];
        colorArray = [[NSMutableArray alloc] init];
        currentColor = [UIColor blackColor];
        currentPath = [self makeNewPath];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    for (int i = 0; i < [pathArray count]; i++) {
        UIBezierPath *path = (UIBezierPath *)pathArray[i];
        UIColor *color = (UIColor *)colorArray[i];
        [color setStroke];
        [path stroke];
    }
    [currentColor setStroke];
    [currentPath stroke];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    [currentPath moveToPoint:p];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    [currentPath addLineToPoint:p];
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self touchesMoved:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self touchesEnded:touches withEvent:event];
}

/// Clear all drawing
- (void)erase {
    [pathArray removeAllObjects];
    [colorArray removeAllObjects];
    currentPath = [self makeNewPath];
    [self setNeedsDisplay];
}

/// Change the color of a drawing brush
- (void)setColor:(UIColor *)color {
    if (color == nil || [color isEqual:currentColor]) {
        return;
    }
    if (![currentPath isEmpty]) {
        [self saveCurrentPath];
    }
    currentColor = color;
    currentPath = [self makeNewPath];
}

- (UIBezierPath *)makeNewPath {
    UIBezierPath *p = [UIBezierPath bezierPath];
    [p setLineWidth: (float) brushSize];
    return p;
}

- (void)saveCurrentPath {
    [pathArray addObject:[currentPath copy]];
    [colorArray addObject:[currentColor copy]];
}

- (void) setBrushSize:(int) size {
    if(size < MIN_BRUSH_WIDTH || size > MAX_BRUSH_WIDTH) {
        return;
    }
    if(size == brushSize) {
        return;
    }
    if (![currentPath isEmpty]) {
        [self saveCurrentPath];
    }
    brushSize = size;
    currentPath = [self makeNewPath];
}

@end
