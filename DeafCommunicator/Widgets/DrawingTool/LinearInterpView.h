//
//  LinearInterpView.h
//  FreehandDrawingTut
//
//  Created by Jordan on 7/1/14.
//  Copyright (c) 2014 Jordan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinearInterpView : UIView
- (void) erase;
- (void) setColor:(UIColor *) color;
- (void) setBrushSize:(int) size;
@end
